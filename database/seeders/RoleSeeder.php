<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Models\RoleList::roleList() as $role) {
            $roles = DB::table('roles')->where('name', $role)->exists();
            if (!$roles) {
                DB::table('roles')->insert([
                    'name' => $role,
                ]);
            }
        }
    }
}
