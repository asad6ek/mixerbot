<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $username = 'jasco-admin';

        $hasSuperAdmin = DB::table('admins')->where('username', $username)->exists();

        if (!$hasSuperAdmin){
            DB::table('admins')->insert([
                'username' => $username,
                'password' => bcrypt('123456'), // password
                'remember_token' => Str::random(10),
            ]);
        }    }
}
