<?php


namespace App\Repositories\Telegram;

use App\Models\BotUser;

class CheckRepo
{
    public function insertBotUser($user, $insert = null)
    { $insertIsNull = null;
        if(!is_null($insert)){
            $insertIsNull = \GuzzleHttp\json_encode($insert);
        }
        $botUser = BotUser::query()
            ->where('chat_id', '=', $user->getId());

        if ($botUser->get()->count()) {

            $botUser->update(['actions' => $insertIsNull]);
        } else {

            $data = [
                'chat_id' => $user->getId(),
                'first_name' => $user->getFirstName(),
                'last_name' => $user->getLastName(),
                'actions' => $insertIsNull,
            ];

            BotUser::query()
                ->insert($data);
        }
    }

    public function selectBotUser($user)
    {
        return BotUser::query()
            ->where('chat_id', '=', $user)
            ->first();
    }


}
