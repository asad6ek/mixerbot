<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-04
 * Time: 16:49
 */

namespace App\View;


use App\View\Traits\ViewComponentOptionAttributeResolverTrait;
use Illuminate\View\ComponentAttributeBag;
use Illuminate\View\InvokableComponentVariable;

class AttributeLister
{
    use ViewComponentOptionAttributeResolverTrait;
    /**
     * @param ComponentAttributeBag $attributes
     * @param array $options
     * @param array $except
     * @return string
     */
    public static function list(ComponentAttributeBag $attributes, $options = [], $except = [])
    {
        $mergedOptions = self::merge($attributes, $options, $except);

        return self::listArray($mergedOptions);
    }

    /**
     * @param $options
     * @return array
     */
    public static function options($options)
    {
        if (!$options) {
            return [];
        }

        if (is_callable($options)) {
            return call_user_func($options);
        } elseif ($options instanceof InvokableComponentVariable) {
            return $options->resolveDisplayableValue();
        }

        return $options;
    }

    /**
     * @param ComponentAttributeBag $attributes
     * @param array $options
     * @param array $except
     * @return ComponentAttributeBag
     */
    public static function merge(ComponentAttributeBag $attributes, $options = [], $except = [])
    {
        $options = static::options($options);
        $except = static::options($except);

        return $attributes->merge((array)$options)->except((array)$except);
    }

    /**
     * @param $array
     * @return string
     */
    public static function listArray($array)
    {
        $text = '';
        foreach ($array as $optionAttribute => $optionValue) {
            $text .= ' ' . $optionAttribute . '="' . $optionValue . '"';
        }
        return $text;
    }

    public static function invokableToVal($item)
    {
        return self::resolveViewDataByValue($item);
    }


}
