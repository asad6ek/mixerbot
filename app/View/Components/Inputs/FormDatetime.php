<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-07-28
 * Time: 16:30
 */

namespace App\View\Components\Inputs;


use Illuminate\Support\Arr;

class FormDatetime extends FormInput
{
    public function options()
    {
        $options = parent::options();

        if (!isset($options['class'])) {
            $options['class'] = '';
        }

        $options['class'] .= ' datepicker';

        if (isset($options['placeholder'])) {
            $options['placeholder'] = 'Укажите дату';
        }

        return $options;
    }
}
