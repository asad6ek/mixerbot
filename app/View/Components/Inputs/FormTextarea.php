<?php

namespace App\View\Components\Inputs;


class FormTextarea extends AbstractFormInput
{


    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.form-text-area', $this->data());
    }

    public function options()
    {
        $options = $this->excludedOptions();
        $options['name'] = $this->attribute();
        $options['content'] = $this->value();

        if (!array_key_exists('class', $options)) {
            $options['class'] = 'form-control';
        }
        return $options;
    }

}
