<?php

namespace App\View\Components\Inputs;


class FormSelect extends FormDropDown
{
    public function options()
    {
        $options = parent::options();
        $options['class'] = 'selectpicker';
        $options['data-style'] = 'btn-default btn-block';
        $options['data-size'] = 7;

        if (isset($options['noplaceholder'])) {
            $options['data-none-selected-text'] = 'Ничего не выбрано';
            $options['title'] = 'Ничего не выбрано';
        }


//        if (!isset($options['prompt']) && !isset($options['noplaceholder'])) {
//            $options['prompt'] = 'Выбрать';
//        }
        //data-style="btn-default btn-block" data-size="7"
        return $options;
    }
}
