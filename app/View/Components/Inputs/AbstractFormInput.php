<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-02
 * Time: 16:51
 */

namespace App\View\Components\Inputs;


use App\Traits\HtmlAttributeName;
use App\View\Interfaces\IFormInputable;
use App\View\Traits\ExcludedOptionsTrait;
use Illuminate\Support\Arr;
use Illuminate\View\Component;

abstract class AbstractFormInput extends Component
{
    use ExcludedOptionsTrait;
    use HtmlAttributeName;
    /**
     * @var IFormInputable
     */
    protected $model;

    protected $attribute;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(IFormInputable $model, $attribute, $options = [])
    {

        $this->model = $model;
        $this->options = $options;
        $this->attribute = $attribute;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {

        return view('components.inputs.form-input', $this->data());
    }

    public function labelOptions()
    {
        $attribute  = self::getAttributeName($this->attribute);
        $label = $this->options['lable'] ?? $this->model->getAttributeLabel($attribute);
        $options = Arr::get($this->options, 'labelOptions', []);
        $options['content'] = $label;
        return $options;
    }

    public function attribute()
    {
        return $this->attribute;
    }

    public function value()
    {
        $attribute  = self::getAttributeName($this->attribute);
        $value = $this->paramOptions()['value'] ?? $this->model->getAttribute($attribute);

        return old($this->attribute(), $value);
    }

    public function divOptions()
    {
        $options = Arr::get($this->options, 'divOptions', []);
        $class = $options['class'] ?? 'form-group';
        $options['class'] = $class;

        return $options;
    }

    public function excludedOptionProperties()
    {
        return ['label', 'labelOptions', 'divOptions', 'value'];
    }

}
