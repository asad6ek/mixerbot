<?php

namespace App\View\Components\Inputs;

use App\View\Interfaces\IFormInputable;
use App\View\Traits\ViewComponentOptionAttributeResolverTrait;
use Illuminate\Support\Arr;

/**
 * Class FormInput
 * @package App\View\Components\Inputs
 *
 * @usage
 *
 * model  is a object which implements IFormInputableInterface
 *
 * <x-inputs.form-input :model="$model" attribute="name" />
 *
 */
class FormInput extends AbstractFormInput
{

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.inputs.form-input', $this->data());
    }

    public function options()
    {
        $options = parent::options();

        if (!array_key_exists('class', $options)){
            $options['class'] = 'form-control';
        }

        return $options;
    }

}
