<?php

namespace App\View\Components;

use Illuminate\Support\Arr;
use Illuminate\View\Component;

class Alert extends Component
{

    private $message;
    private $type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($message, $type = 'danger')
    {
        if (!$this->isCorrectType($type)) {
            throw new \Exception('Unknown alert type: ' . $type);
        }

        $this->message = $message;
        $this->type = $type;
    }

    public static function types()
    {
        return [
            'info' => 'info',
            'success' => 'success',
            'danger' => 'danger',
            'warning' => 'warning',
            'error' => 'danger',
        ];
    }

    protected function isCorrectType($type)
    {
        return in_array(strtolower($type), array_keys(self::types()));
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.alert', $this->data());
    }

    public function type()
    {
        return Arr::get(self::types(), $this->type, $this->type);
    }

    public function messages()
    {
        $message = $this->message;
        if (is_string($message)) {
            $message = [$this->message];
        }
        return $message;
    }
}
