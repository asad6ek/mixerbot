<?php

namespace App\View\Traits;


use Illuminate\Support\Str;

trait FormInputAttributeLabelTrait
{
    /**
     * @param $attribute
     * @return string
     */
    public function getAttributeLabel($attribute)
    {
        $labels = $this->getAttributeLabelValuesFromObj();

        if (array_key_exists($attribute, $labels)) {
            return $labels[$attribute];
        }

        return $this->transferAttributeToLabel($attribute);
    }


    /**
     * @return array
     */
    public function getAttributeLabelValuesFromObj()
    {
        $labels = [];
        if (method_exists($this, 'getAttributeLabels')) {
            $labels = $this->getAttributeLabels();
        } elseif (property_exists($this, 'attributeLabels')) {
            $labels = $this->attributeLabels;
        }

        return $labels;
    }

    /**
     * @param $attribute
     * @return string
     */
    protected function transferAttributeToLabel($attribute)
    {
        $attr = Str::snake($attribute);

        return Str::title(str_replace('_', ' ', $attr));
    }


}
