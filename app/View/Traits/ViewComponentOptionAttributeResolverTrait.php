<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-02
 * Time: 14:52
 */

namespace App\View\Traits;


use Illuminate\View\ComponentAttributeBag;
use Illuminate\View\InvokableComponentVariable;

trait ViewComponentOptionAttributeResolverTrait
{
    protected $options;

    /**
     * @return array
     */
    public function paramOptions()
    {

        if (is_callable($this->options)) {
            return call_user_func($this->options);
        } elseif ($this->options instanceof InvokableComponentVariable) {
            return $this->options->resolveDisplayableValue();
        }

        return $this->options;
    }

    public function resolveViewDataByAttribute($attribute)
    {
        return self::resolveViewDataByValue($this->$attribute);
    }

    public static function resolveViewDataByValue($value)
    {
        if ($value instanceof InvokableComponentVariable) {
            return $value->resolveDisplayableValue();
        }elseif ($value instanceof ComponentAttributeBag){
            return (array) $value->getIterator();
        }elseif (is_callable($value)) {
            return call_user_func($value);
        }

        return $value;
    }
}
