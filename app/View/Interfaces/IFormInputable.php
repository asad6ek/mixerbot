<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-01
 * Time: 17:10
 */

namespace App\View\Interfaces;


interface IFormInputable
{
    public function getAttribute($attribute);

    public function getAttributeLabel($attribute);
}
