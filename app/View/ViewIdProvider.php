<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-01
 * Time: 17:02
 */

namespace App\View;


class ViewIdProvider
{
    static protected $currentId = 0;

    public static function getNewId()
    {
        return self::$currentId++;
    }

}
