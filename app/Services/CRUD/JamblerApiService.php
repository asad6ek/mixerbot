<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-12
 * Time: 16:40
 */

namespace App\Services\CRUD;


use App\Models\ApiKeys;

class JamblerApiService extends BaseCrudService
{
    protected $modelClass = ApiKeys::class;

    public function create($data)
    {
        $count = $this->modelClass::query()->where('default', 1)->count();

        $model = new  $this->modelClass();
        $model->fill($data);

        if (!$count) {
            $model->default = 1;
        }

        $model->save();
        return $model;
    }

    public function updateDefault($apiKeys)
    {
        $default = $this->modelClass::query()->where('default', 1)->first();
        if (!empty($default)) {
            $default->default = 0;
            $default->save();
        }
        $model = $this->modelClass::query()->where('id', $apiKeys)->first();
        $model->default = 1;
        $model->save();
    }
}
