<?php

namespace App\Services\CRUD;


use App\Models\TelegramAccountSetting;
use App\Models\TelegramBotSetting;
use App\Models\TelegramSetting;
use Illuminate\Support\Arr;

class TelegramSettingCrudService
{
    public function save(TelegramSetting $setting, $data)
    {

        $setting = $this->model($setting);

        if (!empty($data['delete_image'])) {
            $this->deleteImage($setting);
        }
        $setting->fill($data);

        $setting->uploadImageByAttribute($data['image'] ?? null, 'image');

        $setting->save();

        return $setting;

    }

    public function deleteImage(TelegramSetting $setting)
    {
        $setting->deleteFile('image');
        $setting->image = null;
    }

    /**
     * @param TelegramSetting $setting
     * @return TelegramSetting
     */
    public function model(TelegramSetting $setting)
    {

        $model = new TelegramSetting();
        $model->fill($setting->toArray());
        if ($setting->id){
            $model->exists = true;
            $model->id = $setting->id;
        }

        return $model;
    }
}
