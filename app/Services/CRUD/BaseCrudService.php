<?php

namespace App\Services\CRUD;


use App\Interfaces\ICrudService;
use App\Models\BaseModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;

abstract class BaseCrudService implements ICrudService
{


    public function list($data = [])
    {

        return $this->query()
            ->filter($data)
            ->paginate();
    }

    public function create($data)
    {
        $class = $this->getModelClass();
        $model = new $class();
        $model->fill($data);
        $model->save();
        return $model;
    }

    public function update($data, $id)
    {
        $model = $this->find($id);
        $model->fill($data);
        $model->save();

        return $model;
    }

    public function delete($id)
    {
        $model = $this->find($id);
        return $model->delete();
    }

    /**
     * @param $id
     * @return mixed
     * @throws \Exception
     * @throws ModelNotFoundException
     */
    public function find($id)
    {
        $class = $this->getModelClass();

        return $class::findOrFail($id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     */
    protected function query()
    {
        $class = $this->getModelClass();

        return $class::query()
            ->orderBy('id', 'desc');
    }

    /**
     * @return BaseModel | string
     * @throws \Exception
     */
    protected function getModelClass()
    {
        if (property_exists($this, 'modelClass')) {
            return $this->modelClass;
        }

        throw new \Exception(get_class($this) . ' modelClass property not implemented');
    }
}
