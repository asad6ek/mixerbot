<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-12
 * Time: 16:40
 */

namespace App\Services\CRUD;


use App\Models\Coupon;
use Illuminate\Support\Str;

class CouponService extends BaseCrudService
{
    protected $modelClass = Coupon::class;

    public function create($data)
    {
        $num = $data['number'] ?? 0;
        $api_id = $data['api_id'] ?? null;

        for ($i = 0; $i < $num; $i++) {
            $model = new Coupon();
            $model->api_id = $api_id;
            $model->code = $this->generateCode();
            $model->save();
        }

        return true;
    }

    private function generateCode(): string
    {
        while (true) {
            $text = Str::random('12');
            if (!Coupon::query()->where('code', $text)->exists()) {
                return $text;
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Exception
     */
    protected function query()
    {
        $class = $this->getModelClass();

        return $class::query()->with('api')
            ->orderBy('id', 'desc');
    }
}
