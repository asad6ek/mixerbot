<?php


namespace App\Services\Jambler;


use App\Models\Coupon;
use App\Models\ApiKeys;
use App\Repos\CouponRepo;
use App\DDD\Jambler\Response\Info;
use App\DDD\Jambler\Response\Order;

class InfoService
{
    protected $apiUrl = 'https://api.jambler.io';
    protected ?Coupon $couponModel = null;

    /**
     * @var CouponRepo
     */
    private CouponRepo $repo;

    /**
     * InfoService constructor.
     *
     * @param CouponRepo $repo
     */
    public function __construct(CouponRepo $repo)
    {
        $this->repo = $repo;
    }

    public function info($coupon = null): Info
    {
        $couponModel = $this->getCouponModel($coupon);

        $jambler_api_key = $this->getToken($couponModel);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_URL            => $this->apiUrl . '/partners/info/btc',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER     => [
                'Cache-Control: no-cache',
                'Content-Type: application/json',
                'xkey: ' . $jambler_api_key,
            ],
        ]);
        $info =(array) json_decode(curl_exec($curl), true);
        $err = curl_error($curl);
        curl_close($curl);

        $model = new Info($info);
        if ($err) {
            $model->setError('Unknown error');
            return $model;
        } elseif ($model->hasError()) {
            return $model;
        }

        $info['order_lifetime'] = floor($info['order_lifetime'] / 24);
        $info['mixer_fix_fee'] = round($info['mixer_fix_fee'] / 100000000, 5);
        $info['min_amount'] = round($info['min_amount'] / 100000000, 5);
        $info['max_amount'] = round($info['max_amount'] / 100000000, 5);

        return new Info($info);
    }


    public function crud($data, $coupon = null)
    {
        $couponModel = $this->getCouponModel($coupon);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_CUSTOMREQUEST  => 'POST',
            CURLOPT_URL            => $this->apiUrl . '/partners/orders/btc',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER     => [
                'Cache-Control: no-cache',
                'Content-Type: application/json',
                'xkey: ' . $this->getToken($couponModel),
            ],
            CURLOPT_POSTFIELDS     => json_encode([
                'forward_addr'  => $data['forward_addr'] ?? null,
                'forward_addr2' => $data['forward_addr2'] ?? null,
            ]),
        ]);
        $res = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);

        $order = new Order($res);
        curl_close($curl);
        if ($err) {
            $order->setError('Unknown error');
            return $order;
        } elseif ($order->hasError()) {
            return $order;
        }

        if (!$order->hasError() && $couponModel) {
            $this->repo->makeUsed($couponModel);
        }

        return $order;
    }

    public function select($data)
    {
        dd('galdi');
        $proto = !empty($_SERVER['HTTP_X_FORWARDED_PROTO']) ? $_SERVER['HTTP_X_FORWARDED_PROTO'] : (!empty($_SERVER['HTTPS']) ? 'https' : 'http');
        $host = strrchr($_SERVER['HTTP_HOST'], '.') == '.onion' ? 'http://localhost:8000' : $proto . '://' . $_SERVER['HTTP_HOST'];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL            => $host . '/libs/bitcoin-address-validator.php?address=' . $data,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => '',
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => 'GET',
        ]);
        $res = curl_exec($curl);
        $err = curl_error($curl);
        if ($err) {
            dd($err);
        }

    }

    public function getToken(Coupon $coupon = null): ?string
    {
        if ($coupon) {
            return $coupon->api->api_key ?? null;
        }

        $apikey = ApiKeys::query()->where('default',1)->first();
        if (!$apikey) {
            return null;
        }
        return $apikey->api_key;
    }

    protected function getCouponModel($coupon)
    {
        if ($coupon && $this->couponModel === null) {
            $this->couponModel = $this->repo->find($coupon);
        }

        return $this->couponModel;
    }
}
