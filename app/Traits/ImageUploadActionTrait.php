<?php


namespace App\Traits;


use App\Http\Requests\ImageUploadActionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait ImageUploadActionTrait
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     *
     */
    public function upload(ImageUploadActionRequest $request)
    {
        if ($request->hasFile('upload')) {
            //get filename with extension
            $filenamewithextension = $request->file('upload')->getClientOriginalName();

            //get file extension
            $extension = $request->file('upload')->getClientOriginalExtension();

            //filename to store
            $filenametostore = \Shop::id() . Str::random(32) . '.' . $extension;

            //Upload File
            $request->file('upload')->storeAs('public/uploads', $filenametostore);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('storage/uploads/' . $filenametostore);
            $msg = 'Image successfully uploaded';
            $re = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";

            // Render HTML output
            @header('Content-type: text/html; charset=utf-8');
            echo $re;
        }

    }
}
