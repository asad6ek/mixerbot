<?php

namespace App\Traits;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

trait ProfilePasswordChangeTrait
{
    public function index()
    {
        $user = $this->user();
        abort_if(!$user, 'Not found');

        return $this->render($this->viewForm());
    }

    public function store(Request $request)
    {
        $user = $this->user();
        abort_if(!$user, 'Not found');

        $validator = Validator::make($request->all(), [
            'old-password' => [
                'required',
                'min:6'
            ],
            'new_password' => [
                'required',
                'confirmed',
                'min:6',
            ],
        ]);

        $validator->after(function ($validator) use ($user, $request) {
            $hasher = app('hash');
            if (!$hasher->check($request->input('old-password'), $user->password)) {
                $validator->errors()->add('old-password', 'Не верный Старый пароль');
            }
        });


        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $user->password = bcrypt($request->input('new_password'));
        $user->save();

        alertMessage('success', trans('messages.updated_successfully',['main'=>'Профиль']));

        return redirect()->back();
    }

    public function viewForm()
    {
        return 'common.admin-side.profile.edit';
    }

    public function render($view, $data = [])
    {
        return view($view, $data);
    }

    public function user()
    {
        return Auth::guard()->user();
    }
}
