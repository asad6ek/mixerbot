<?php

namespace App\Traits;


use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

trait AjaxValidationExceptionRuleTrait
{
    protected function failedValidation(Validator $validator)
    {

        if ($this->ajax()) {
            throw new HttpResponseException(new JsonResponse([
                'success' => false,
                'errors' => $validator->errors(),
            ]));
        }

        parent::failedValidation($validator);
    }
}
