<?php

namespace App\Traits;


trait ModelListTrait
{
    public static function list($status = false)
    {
        $name = 'name';
        if (property_exists(self::class, 'list_name_attribute')) {
            $name = self::$list_name_attribute;
        }

        $id = 'id';
        if (property_exists(self::class, 'list_key_attribute')) {
            $id = self::$list_key_attribute;
        }

        $query = self::query();

        if ($status) {
            $query->where('status', $status);
        }

        return $query->pluck($name, $id);
    }
}
