<?php

namespace App\Traits;


use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait UploadImageTrait
{
    public function uploadImageByAttribute(UploadedFile $file = null, $attribute = 'pic')
    {

        if (!$file) {
            return;
        }
        $extention = $file->clientExtension();
        $name = Str::random(32);
        $fileName = $name . '.' . $extention;
        $fileWithPath = $this->uploadFilePath($attribute);
        $this->deleteFile($attribute);
        if (!$this->uploadStorage()->exists($fileWithPath)) {
            $this->uploadStorage()->createDir($fileWithPath);
        }
        $this->uploadStorage()->putFileAs($fileWithPath, $file, $fileName);
//        $file->storeAs($fileWithPath, $fileName, [
//            'visibility' => 'public'
//        ]);
        $this->{$attribute} = $fileWithPath . '/' . $fileName;
    }

    public function uploadFilePath($attribute = 'pic')
    {
        return class_basename(get_class($this));
    }

    public function fileUrl($attribute = 'pic')
    {
        return '/uploads/' . $this->{$attribute};
    }

    /**
     * @return \Illuminate\Contracts\Filesystem\Filesystem|\Illuminate\Filesystem\FilesystemAdapter
     */
    public function uploadStorage()
    {
        return Storage::disk('my_files');
    }

    public function deleteFile($attribute = 'pic')
    {
        if ($this->{$attribute} && $this->uploadStorage()->exists($this->{$attribute})) {
            $this->uploadStorage()->delete($this->{$attribute});
        }
    }

}
