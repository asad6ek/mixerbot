<?php

namespace App\Traits;


use App\View\Traits\FormInputAttributeLabelTrait;

trait ClassFormInputableTrait
{
    use FormInputAttributeLabelTrait;

    public function getAttribute($attribute)
    {
        return $this->{$attribute};
    }
}
