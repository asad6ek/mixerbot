<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-07-03
 * Time: 12:47
 */

namespace App\Interfaces;


interface ICommand
{
    public function execute();
}
