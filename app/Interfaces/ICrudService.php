<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-12
 * Time: 16:29
 */

namespace App\Interfaces;


use Illuminate\Http\Request;

interface ICrudService
{
    public function list($data = []);

    public function create($data);

    public function update($data, $id);

    public function delete($id);

    public function find($id);
}
