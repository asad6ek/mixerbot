<?php


namespace App\Repos;


use Carbon\Carbon;
use App\Models\Coupon;

class CouponRepo
{
    public function find($code)
    {
        return Coupon::query()
            ->active()
            ->where('code', $code)
            ->first();
    }

    public function save(Coupon $coupon)
    {
        $coupon->save();
        return $coupon;
    }

    public function makeUsed(Coupon $coupon)
    {
        if ($coupon->isUsed()) {
            throw new \Exception('Coupon has been used Already');
        }

        $coupon->used_at = Carbon::now();
        $this->save($coupon);
    }
}
