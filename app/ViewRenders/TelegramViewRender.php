<?php

namespace App\ViewRenders;


class TelegramViewRender
{

    public function render($data, $purser)
    {

        return view('telegramBot.'.$purser, $data)->toHtml();
    }
}
