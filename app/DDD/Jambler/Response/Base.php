<?php


namespace App\DDD\Jambler\Response;


abstract class Base implements \ArrayAccess
{
    protected ?string $error = null;
    protected array $attributes = [];

    public function __construct(array $data)
    {
        $this->handleResponse($data);
    }

    public function hasError(): bool
    {
        return trim($this->error) !== '';
    }

    public function getErrorMessage()
    {
        return $this->error;
    }

    public function setError($error): void
    {
        $this->error = $error;
    }

    protected function setAttributes($attributes): void
    {
        $this->attributes = $attributes;
    }

    protected function handleResponse($data)
    {
        if (!empty($data['error_message'])) {
            $this->setError($data['error_message']);
            return;
        }

        $this->setAttributes($data);
    }

    public function __get($name)
    {
        return $this->offsetGet($name);
    }


    public function __isset($name)
    {
        return $this->offsetExists($name);
    }

    public function offsetExists($offset)
    {
        return isset($this->attributes[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->attributes[$offset] ?? null;
    }

    public function offsetSet($offset, $value)
    {
        $this->attributes[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->attributes[$offset]);
            return;
        }
    }
}
