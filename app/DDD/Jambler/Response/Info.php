<?php


namespace App\DDD\Jambler\Response;

/**
 * Class Info
 *
 * @package App\DDD\Jambler\Response
 *
 * @property string $mixer_name
 * @property string $guarantee_lang
 * @property string $econ_mixer_fee_pct
 * @property integer $mixer_fix_fee
 * @property integer $econ_mixer_fix_fee
 * @property integer $order_lifetime
 * @property integer $withdraw_max_timeout
 * @property integer $min_amount
 * @property integer $max_amount
 * @property integer $econ_min_amount
 * @property integer $econ_max_amount
 * @property integer $econ_withdraw_max_timeout
 */
class Info extends Base
{

}
