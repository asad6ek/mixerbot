<?php


namespace App\DDD\Jambler\Response;

/**
 * Class Order
 *
 * @package App\DDD\Jambler\Response
 * @property string  $guarantee
 * @property string  $address
 * @property string  $key
 * @property integer $min_amount
 * @property string  $forward_addr
 * @property string  $forward_addr2
 * @property integer $forward_amount
 */
class Order extends Base
{

}
