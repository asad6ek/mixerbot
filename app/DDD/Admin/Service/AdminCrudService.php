<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-04
 * Time: 17:33
 */

namespace App\DDD\Admin\Service;


use App\Models\Admin;
use App\Models\Shop;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;

class AdminCrudService
{
    public function list($data = [])
    {
        return Admin::query()
            ->paginate($data['perPage'] ?? null);
    }

    public function create($data, Shop $shop)
    {
        $model = new Admin();

        $model->fill($data);
        $model->password = bcrypt($data['password'] ?? '123456');
        $model->setRememberToken(Str::random(32));

        $shop->admins()->save($model);
        return $model;
    }

    public function update($data, $id)
    {
        $model = $this->find($id);

        $model->fill($data);

        if (!empty($data['password'])) {
            $model->password = bcrypt($data['password']);
        }

        $model->save();

        return $model;

    }

    /**
     * @param $id
     * @return Admin
     * @throws ModelNotFoundException
     */
    public function find($id)
    {
        return Admin::query()
            ->findOrFail($id);
    }
}
