<?php

namespace App\ViewModels;

use App\Models\Answer;
use Spatie\ViewModels\ViewModel;

class MixerListViewModel extends ViewModel
{


    private $info;
    private $error;

    public function __construct($info, $error)
    {

        $this->info = $info;
        $this->error = $error;

    }

  public function info(){
        return $this->info;
  }

  public function models(){
        return Answer::query()
            ->where('status',1)
            ->get();
  }
  public function error(){
        return $this->error;
  }
}
