<?php


namespace App\ViewModels;


use App\Models\ApiKeys;
use Spatie\ViewModels\ViewModel;


class CuponViewModel extends ViewModel
{

    private $model;

    public function __construct($model)
    {

        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function model()
    {
        return $this->model;
    }

    public function apiList()
    {
        return ApiKeys::all();
    }
}
