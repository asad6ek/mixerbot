<?php

namespace App\ViewModels;

use App\Models\TelegramSetting;
use Spatie\ViewModels\ViewModel;

class TelegramSettingsViewModel extends ViewModel
{
    /**
     * @var TelegramSetting
     */
    private $setting;

    public function __construct(TelegramSetting $setting)
    {
        $this->setting = $setting;
    }

    public function model()
    {
        return $this->setting;
    }

}
