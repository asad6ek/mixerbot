<?php

namespace App\Events;

use App\Models\TelegramSetting;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ActivateAccountEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var TelegramSetting
     */
    protected $setting;

    /**
     * @return TelegramSetting
     */
    public function getSetting(): TelegramSetting
    {
        return $this->setting;
    }

    /**
     * Create a new event instance.
     *
     * @param TelegramSetting $setting
     */
    public function __construct(TelegramSetting $setting)
    {
        //
        $this->setting = $setting;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
