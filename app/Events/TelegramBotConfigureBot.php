<?php

namespace App\Events;

use App\Models\TelegramSetting;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class TelegramBotConfigureBot
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var TelegramSetting
     */
    public TelegramSetting $setting;

    /**
     * Create a new event instance.
     *
     * @param TelegramSetting $setting
     */
    public function __construct(TelegramSetting  $setting)
    {
        $this->setting = $setting;
    }

}
