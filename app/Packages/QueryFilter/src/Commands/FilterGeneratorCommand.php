<?php

namespace QueryFilter\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;

class FilterGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:filter {name} {--model=} {--O|object}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate filter for model';


    protected $type = 'Filters';


    /**
     * The name of class being generated.
     *
     * @var string
     */
    private $filterClass;


    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {

        $this->setFilterClass();


        $path = $this->getPath($this->type . '/' . $this->filterClass);

        $file = $this->rootNamespace() . $this->type . '\\' . $this->filterClass;

        if ($this->alreadyExists($file)) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        if (!$this->checkModelClass()) {
            $this->error($this->getModelClass() . ' does not exists!');
            return false;
        }


        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($file));

        if ($this->option('object')) {

            \Artisan::call('make:filter-holder', [
                'name' => $this->argument('name'),
                '--model' => $this->option('model'),
            ]);
        }

        $this->info(Str::singular($this->type) . ' created successfully.');

        $this->line("<info>Created Filter :</info> $this->filterClass");
    }

    /**
     * Set repository class name
     *
     */
    private function setFilterClass()
    {
        $name = ucwords(strtolower($this->argument('name')));

        $this->filterClass = $name . 'Filter';

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        if (!$this->argument('name')) {
            throw new InvalidArgumentException("Missing required argument model name");
        }

        $stub = parent::replaceClass($stub, $name);

        $stub = $this->addFilters($stub);


        return $stub;
    }

    /**
     *
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../views/stubs/Filter.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Filters';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model class.'],
        ];
    }


    public function addFilters($stubs)
    {

        if (!$this->option('model')) {
            return $stubs;
        }

        $attibutesList = $this->getModelAttributes();

        $functionList = '';

        foreach ($attibutesList as $attr_name => $type) {

            $excludedAttributes = (array)config('filter.exclude', ['shop_id', 'deleted_at']);
            if (in_array($attr_name, $excludedAttributes)) {
                continue;
            }

            $text = "\tpublic  function $attr_name(\$value)\r\t{\r\n";

            if ($type === 'string') {
                $text .= "\t\treturn \$this->builder->where('$attr_name','like', '%'.\$value .'%');";
            } else {
                $text .= "\t\treturn \$this->builder->where('$attr_name', \$value);";
            }

            $text .= "\r\n\t} \r\n";

            $functionList .= $text . "\r\n";
        }

        return str_replace('// methods', $functionList, $stubs);
    }

    protected function getModelAttributes()
    {

        $attributeList = [];

        $class = $this->getModelClass();
        /**
         * @var $model Model
         */
        $model = new $class;

        $attribues = $model->getConnection()->getDoctrineSchemaManager()->listTableColumns($model->getTable());

        foreach ($attribues as $attr_name => $column) {
            $type = $column->getType()->getName();

            switch ($type) {
                case 'string':
                case 'text':
                    $type = 'string';
                    break;
                case 'date':
                case 'time':
                    $type = 'date';
                    break;
                case 'guid':
                case 'datetimetz':
                case 'datetime':
                    $type = 'datetime';
                    break;
                case 'boolean':
                case 'integer':
                case 'bigint':
                case 'smallint':
//                    $type = 'integer';
//                    break;
                case 'decimal':
                case 'float':
                    $type = 'number'; // 'float'
                    break;
                case 'json':
                    $type = 'json';
                    break;
                default:
                    $type = 'mixed';
                    break;
            }

            $attributeList[$attr_name] = $type;
        }

        return $attributeList;
    }

    public function getModelClass()
    {
        return $this->rootNamespace() . str_replace('/', '\\', $this->option('model'));

    }

    public function checkModelClass()
    {
        if (!$this->option('model')) {
            return true;
        }

        $class = $this->getModelClass();

        if (!class_exists($class)) {
            return false;
        }

        return true;
    }
}
