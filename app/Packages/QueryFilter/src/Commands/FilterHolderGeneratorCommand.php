<?php

namespace QueryFilter\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;

class FilterHolderGeneratorCommand extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:filter-holder {name} {--model=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate filter holder for model';


    protected $type = 'FilterHolders';


    /**
     * The name of class being generated.
     *
     * @var string
     */
    private $holderClass;


    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {

        $this->setHolderClass();


        $path = $this->getPath($this->getFilePath() . '/' . $this->holderClass);

        $file = str_replace('/','\\',$this->rootNamespace() . $this->getFilePath() . '\\' . $this->holderClass);

        if ($this->alreadyExists($file)) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        if (!$this->checkModelClass()) {
            $this->error($this->getModelClass() . ' does not exists!');
            return false;
        }


        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($file));

        $this->info(Str::singular($this->type) . ' created successfully.');

        $this->line("<info>Created Filter holder :</info> $this->holderClass");
    }

    /**
     * Set repository class name
     *
     */
    private function setHolderClass()
    {
        $name = ucwords(strtolower($this->argument('name')));


        $this->holderClass = $name . 'FilterHolder';

        return $this;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param  string $stub
     * @param  string $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        if (!$this->argument('name')) {
            throw new InvalidArgumentException("Missing required argument model name");
        }

        $stub = parent::replaceClass($stub, $name);

        $stub = $this->addProperties($stub);


        return $stub;
    }



    /**
     *
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../views/stubs/Holder.stub';
    }

    public function getFilePath()
    {
        return 'Filters/Holders';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . 'Filters\Holders';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the model class.'],
        ];
    }


    public function addProperties($stubs)
    {

        if (!$this->option('model')) {
            return $stubs;
        }

        $attibutesList = array_keys($this->getModelAttributes());

        $functionList = '';

        foreach ($attibutesList as  $attr_name) {

            $excludedAttributes = (array)config('filter.exclude', ['shop_id', 'deleted_at']);
            if (in_array($attr_name, $excludedAttributes)) {
                continue;
            }

            $functionList .= "\tpublic \$$attr_name;\r\t\r\n";

        }

        return str_replace('// $items', $functionList, $stubs);
    }

    protected function getModelAttributes()
    {


        $class = $this->getModelClass();
        /**
         * @var $model Model
         */
        $model = new $class;

        return $model->getConnection()->getDoctrineSchemaManager()->listTableColumns($model->getTable());
    }

    public function getModelClass()
    {
        return $this->rootNamespace() . str_replace('/', '\\', $this->option('model'));

    }

    public function checkModelClass()
    {
        if (!$this->option('model')) {
            return true;
        }

        $class = $this->getModelClass();

        if (!class_exists($class)) {
            return false;
        }

        return true;
    }
}
