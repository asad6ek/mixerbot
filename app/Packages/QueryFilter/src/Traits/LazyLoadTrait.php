<?php

namespace QueryFilter\Traits;

trait LazyLoadTrait
{
    public function load($items, $key = '')
    {
        $items = $key ? ($items[$key] ?? []) : $items;

        $loaded = false;

        $properties = $this->getClassProperites();

        foreach ($items as $attribute => $value) {
            if (in_array($attribute, $properties)) {
                $this->{$attribute} = $value;
                $loaded = true;
            }
        }
        return $loaded;

    }

    public function getClassProperites()
    {
        $ref = new \ReflectionClass($this);

        $properties = $ref->getProperties(\ReflectionProperty::IS_PUBLIC);
        $items = [];
        foreach ($properties as $property) {
            $items[] = $property->getName();
        }

        return $items;
    }
}
