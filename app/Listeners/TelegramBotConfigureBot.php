<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram\Bot\BotsManager;

class TelegramBotConfigureBot
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(\App\Events\TelegramBotConfigureBot $event)
    {
        $settings = $event->setting;

        $configs = [
            'bots' => [
                'mybot' => [
                    'username' => 'TelegramBot',
                    'token' => $settings['token'],

                ],

                //        'mySecondBot' => [
                //            'username'  => 'AnotherTelegram_Bot',
                //            'token' => '123456:abc',
                //        ],
            ],
            'default' => 'mybot',
        ];

        app()->bind(BotsManager::class, static function ($app) use ($configs) {
            return (new BotsManager($configs))->setContainer($app);
        });
    }
}
