<?php


namespace App\Requests;


use App\Models\ApiKeys;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MixerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'forward_addr' => [
                'required',
            ],

            'forward_addr2' => [
            ],
            'coupon'        => [
                'nullable',
                Rule::exists('coupons', 'code')
                    ->whereNull('used_at'),
            ],
        ];
    }

    public function attributes()
    {
        $address = new ApiKeys();

        return $address->getAttributeLabelValuesFromObj();
    }
}
