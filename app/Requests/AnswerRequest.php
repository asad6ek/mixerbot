<?php


namespace App\Requests;


use App\Models\Answer;
use Illuminate\Foundation\Http\FormRequest;

class AnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'question' =>
                [
                    'required',
                ],

            'answer' =>
                [
                    'required',
                ],
            'status' =>
                [
                    'required',
                ]
        ];
    }

    public function attributes()
    {
        $address = new Answer();

        return $address->getAttributeLabelValuesFromObj();
    }
}
