<?php


namespace App\Requests;


use App\Models\ApiKeys;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class JamblerApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'api_key' =>
                [
                    'required',
                ],

            'name' =>
                [
                    'required',
                ],
        ];
    }

    public function attributes()
    {
        $address = new ApiKeys();

        return $address->getAttributeLabelValuesFromObj();
    }
}
