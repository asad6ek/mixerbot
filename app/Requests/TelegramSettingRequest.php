<?php

namespace App\Requests;

use App\Models\TelegramSetting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class TelegramSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'status' => [
                'boolean',
            ],
            'token' => [
                'required',
            ],

            'text' => [
                'nullable',
            ],
            'image' => [
                'nullable',
                'image',
            ],
        ];
    }


    public function attributes()
    {
        $address = new TelegramSetting();

        return $address->getAttributeLabelValuesFromObj();
    }
}
