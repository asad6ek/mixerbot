<?php

namespace App\Http\Controllers;


use App\Models\Admin;
use App\Traits\ViewRenderTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;

class AdminController extends \Illuminate\Routing\Controller
{
    use DispatchesJobs, ValidatesRequests;
    use ViewRenderTrait;
    use AuthorizesRequests;

    /**
     * @return Admin
     */
    protected function admin()
    {
        return Auth::guard('admin')->user();
    }

}
