<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Services\Jambler\InfoService;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('coupon.index', (new InfoService())->info());
    }

    public function useCoupon()
    {

    }


}
