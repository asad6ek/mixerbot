<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Requests\MixerRequest;
use App\Services\Jambler\InfoService;
use App\ViewModels\MixerListViewModel;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class MixerController extends Controller
{
    /**
     * @var InfoService
     */
    private InfoService $service;

    /**
     * MixerController constructor.
     *
     * @param InfoService $service
     */
    public function __construct(InfoService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $info = $this->service->info();
        return view('main.index', compact('info'));
    }

    public function create()
    {
        $info = $this->service->info();
        $error = '';
        return \view('main.index', new MixerListViewModel($info, $error));
    }

    public function store(MixerRequest $request)
    {
        $data = $request->validated();
        $coupon = $data['coupon'] ?? null;
        $order = $this->service->crud($data, $coupon);
        $info = $this->service->info($coupon);

        $error = $order->getErrorMessage();

        if ($order->hasError()) {
            return \view('main.index', compact('order', 'info', 'error'));
        }

        return \view('main.result', compact('order', 'info', 'error'));
    }

    public function download(Request $request)
    {

        header('Content-type: text/plain');
        header('Content-Disposition: attachment; filename="LetterGuarantee.txt"');
        echo $request['text'];
    }
}
