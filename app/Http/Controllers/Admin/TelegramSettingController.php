<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController as Controller;
use App\Requests\TelegramSettingRequest;
use App\Models\TelegramSetting;
use App\Services\CRUD\TelegramSettingCrudService;
use App\ViewModels\TelegramSettingsViewModel;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class TelegramSettingController extends Controller
{
    public function index()
    {
        return $this->render('telegram.index', new TelegramSettingsViewModel($this->settings()));
    }

    public function store(TelegramSettingRequest $request, TelegramSettingCrudService $service)
    {
        $data = $request->validated();
        $telegram = $service->save($this->settings(), $data);
//        alertMessage('success', trans('messages.updated_successfully', [ 'main' => 'Настройки телеграм' ]));
//        Telegram::setWebhook([
//            'url' => 'https://' . config('mixer.main_site') . '/telegram/' . $data['token']
//        ]);

        try {

            $client = new Client();

            $client->get(sprintf('https://api.telegram.org/bot%s/setWebhook', $data['token']), [
                'query' => [
                    'url' => 'https://' . config('mixer.main_site') . '/telegram/' . $data['token'],
                ]
            ]);
        } catch (RequestException $exception) {
            echo 'error on api';
            die;
        }
        alertMessage('success', trans('messages.created_successfully'));
        return redirect()->back();
    }

    protected function settings(): TelegramSetting
    {
        return TelegramSetting::query()->first() ?: new TelegramSetting();
    }
}
