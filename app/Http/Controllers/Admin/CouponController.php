<?php

namespace App\Http\Controllers\Admin;

use App\Models\ApiKeys;
use App\Services\CRUD\CouponService;
use App\Http\Controllers\AdminController as Controller;
use App\Models\Coupon;
use App\ViewModels\CuponViewModel;
use Illuminate\Http\Request;

class CouponController extends Controller
{

    /**
     * @var CouponService
     */
    private $service;

    public function __construct(CouponService $service)
    {
        $this->service = $service;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->service->list();
        return $this->render('coupon.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Coupon();
        return $this->render('coupon.create', new CuponViewModel($model));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'api_id' => 'required|exists:api_keys,id',
            'number' => 'required|numeric|max:30|min:1',
        ]);
        $this->service->create($request->all());
        alertMessage('success', trans('messages.created_successfully',['main'=>'']));
        return redirect()->route('admin.coupon.index');
    }



//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param Coupon $id
//     *
//     * @return void
//     */
//    public function edit(Coupon $coupon)
//    {
//        $model = $coupon;
//
//        return $this->render('coupon.update', compact('model'));
//
//    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $data = $request->all();

        $this->service->update(['coupon_key' => $data['coupon_key']], $coupon->id);

        return redirect()->route('admin.coupon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Coupon $coupon)
    {
        if ($coupon->isUsed()) {
            return redirect()->route('admin.coupon.index');
        }

        if ($coupon->delete() && $request->ajax()) {
            return [
                'redirect' => route('admin.coupon.index'),
            ];
        }
        return redirect()->route('admin.coupon.index');
    }
}
