<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController as Controller;
use App\Models\ApiKeys;
use App\Requests\JamblerApiRequest;
use App\Services\CRUD\JamblerApiService;
use Illuminate\Http\Request;

class JamblerApiController extends Controller
{

    /**
     * @var JamblerApiService
     */
    private $service;

    public function __construct(JamblerApiService $service)
    {
        $this->service = $service;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->service->list();
        return $this->render('jambler_api.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new ApiKeys();

        return $this->render('jambler_api.create', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(JamblerApiRequest $request)
    {
        $data = $request->validated();
        $this->service->create($data);
        alertMessage('success', trans('messages.created_successfully',['main'=>'']));
        return redirect()->route('admin.jambler_api.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(ApiKeys $id)
    {
        return $id->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ApiKeys $id
     *
     * @return void
     */
    public function edit(ApiKeys $api)
    {
        $model = $api;

        return $this->render('jambler_api.update', compact('model'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(JamblerApiRequest $request, ApiKeys $api)
    {

        $data = $request->validated();
        $this->service->update($data, $api->id);
        alertMessage('success', trans('messages.updated_successfully',['main'=>'']));
        return redirect()->route('admin.jambler_api.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ApiKeys $api)
    {
        if (ApiKeys::count() > 1 && $api->delete() && $request->ajax()) {
            $api->coupons()->delete();
            return [
                'redirect' => route('admin.jambler_api.index'),
            ];
        }
        return redirect()->route('admin.jambler_api.index');
    }


    public function defaultUpdate($api){
        $this->service->updateDefault($api);
        return redirect()->route('admin.jambler_api.index');

    }
}
