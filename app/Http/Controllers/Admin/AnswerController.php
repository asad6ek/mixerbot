<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController as Controller;
use App\Models\Answer;
use App\Requests\AnswerRequest;
use App\Services\CRUD\AnswerService;
use App\Services\CRUD\JamblerApiService;
use Illuminate\Http\Request;

class AnswerController extends Controller
{

    /**
     * @var JamblerApiService
     */
    private $service;

    public function __construct(AnswerService $service)
    {
        $this->service = $service;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = $this->service->list();
        return $this->render('answer.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Answer();

        return $this->render('answer.create', ['model' => $model]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(AnswerRequest $request)
    {
        $data = $request->validated();
        $this->service->create($data);

        alertMessage('success', trans('messages.created_successfully',['main'=>'']));
        return redirect()->route('admin.answer.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $id)
    {
        return $id->toArray();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Answer $answer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Answer $answer)
    {
        $model = $answer;

        return $this->render('answer.update', compact('model'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param AnswerRequest $request
     *
     * @param Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function update(AnswerRequest $request, Answer $answer)
    {

        $data = $request->validated();
        $this->service->update($data, $answer->id);
        alertMessage('success', trans('messages.updated_successfully',['main'=>'']));
        return redirect()->route('admin.answer.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Answer $answer)
    {

        $answer->delete();
        if ($request->ajax()) {

            return [
                'redirect' => route('admin.answer.index'),
            ];
        }
        return redirect()->route('admin.answer.index');
    }


}
