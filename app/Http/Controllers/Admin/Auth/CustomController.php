<?php

namespace App\Http\Controllers\Admin\Auth;


use App\Http\Controllers\AdminController;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomController extends AdminController
{
    public function __invoke(Request $request, Admin $user, $token)
    {
        abort_if((int)$user->shop_id !== (int)\Shop::id(), 403);

        abort_if($user->enter_token !== $token, 403);

        abort_if(!trim($token), 403);

        $user->enter_token = null;
        $user->save();

        Auth::guard('admin')->login($user);

        return redirect()->route('admin.dashboard');
    }
}
