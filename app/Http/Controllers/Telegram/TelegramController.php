<?php

namespace App\Http\Controllers\Telegram;


use App\Models\BotUser;
use App\Models\Coupon;

use App\Repositories\Telegram\CheckRepo;
use App\Services\Jambler\InfoService;
use App\ViewRenders\TelegramViewRender;
use App\Http\Controllers\Controller;
use App\Models\TelegramSetting;
use Telegram\Bot\BotsManager;
use Telegram\Bot\FileUpload\InputFile;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Update;

class TelegramController extends Controller
{

    /**
     * @var InfoService
     */
    private InfoService $service;
    /**
     * @var TelegramViewRender
     */
    private TelegramViewRender $viewRender;
    /**
     * @var CheckRepo
     */
    private CheckRepo $checkRepo;

    /**
     * MixerController constructor.
     *
     * @param InfoService $service
     */
    public function __construct(InfoService $service, TelegramViewRender $viewRender)
    {
        $this->service = $service;
        $this->viewRender = $viewRender;
        $this->checkRepo = new CheckRepo();

    }


    public function process($token)
    {
        /**
         * @var $settings TelegramSetting
         */
        $settings = TelegramSetting::where(['token' => $token])->first();
        if (!$settings) {
            return 'ok';
        }
        $settings->reconfigure();
        $viewRender = new TelegramViewRender();
        $update = Telegram::getWebhookUpdates();

        /**
         * @var $update Update
         */
        $message = $update->getMessage();

        $user = $message->getChat();
        $user_id = $user->getId();

        if (!$this->isbotActive($settings)) {
            $this->telegramSendMessage($viewRender->render([], 'off'), $user_id);
            return 'ok';
        }

        $checkRepo = new CheckRepo();

        $text = $message->getText();

        if (isset($text)) {
            $mainKeyboards = Keyboard::make(['resize_keyboard' => true])->row(Keyboard::button(['text' => "Mix My Coins"]), Keyboard::button(['text' => "FAQ"]));
            $mainKeyboards = $mainKeyboards->row(Keyboard::button(['text' => "Fee"]), Keyboard::button(['text' => "Contact Us"]));
            $selectBotUser = $checkRepo->selectBotUser($user->getId());
            if (is_null($selectBotUser)) {
                $checkRepo->insertBotUser($user);
            }

            if ($text === '/start') {
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'main_menu'), $user_id, $mainKeyboards);
                exit();
            }
            if ($text === 'Mix My Coins') {
                $checkRepo->insertBotUser($user, ['step' => 'mix_my_coins']);
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'mix_my_coins'), $user_id);
                exit();
            }
            if ($text === 'Fee') {
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'fee_menu'), $user_id);
                $checkRepo->insertBotUser($user);
                exit();
            }
            if ($text === 'FAQ') {
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'faq_menu'), $user_id);
                $checkRepo->insertBotUser($user);

                exit();
            }
            if ($text === 'Contact Us') {
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'contact_us'), $user_id);
                $checkRepo->insertBotUser($user);
                exit();
            }
            if ($text === 'Start Mixing') {

                if ($selectBotUser->actions['step'] !== 'coupon') {
                    $forward_addr = null;

                    if (isset($selectBotUser->actions['forward_addr'])) {
                        $forward_addr = $selectBotUser->actions['forward_addr'];
                    }
                    $checkRepo->insertBotUser($user, [
                        'step' => 'coupon',
                        'forward_addr' => $forward_addr,
                        'forward_addr2' => null,
                        'coupon' => null
                    ]);

                    $this->telegramSendMessage($viewRender->render([], 'coupon.index'), $user_id);
                    exit();
                }

                $this->startMixing($selectBotUser, $user_id, $mainKeyboards, $user);
                exit();

            }
            if ($text === 'Cancel Mixing') {
                $this->telegramSendMessage($viewRender->render(['info' => $this->service->info()], 'main_menu'), $user_id, $mainKeyboards);
                exit();
            }


            if (($selectBotUser->actions)) {

                $step = ($selectBotUser->actions);

                switch ($step['step']) {

                    case 'mix_my_coins':
                    {
                        $data['forward_addr'] = $text;
                        $data['forward_addr2'] = null;
                        $coupon = null;

                        $order = $this->service->crud($data, $coupon);
                        $info = $this->service->info($coupon);
                        $error = $order->getErrorMessage();
                        $keyboard = null;

                        if (!$order->hasError()) {
                            $keyboard = Keyboard::make(['resize_keyboard' => true])->row(Keyboard::button(['text' => "Start Mixing"]), Keyboard::button(['text' => "Cancel Mixing"]));
                            $checkRepo->insertBotUser($user, [
                                'step' => 'mix_my_coins2',
                                'forward_addr' => $text
                            ]);

                        }

                        $this->telegramSendMessage($viewRender->render(['order' => $order, 'info' => $info, 'error' => $error], 'forvard_addr'), $user_id, $keyboard);

                        break;
                    }
                    case 'mix_my_coins2':
                    {
                        $data['forward_addr'] = $text;
                        $order = $this->service->crud($data, null);
                        $error = $order->getErrorMessage();

                        if ($order->hasError()) {
                            $this->telegramSendMessage($this->viewRender->render(['error' => $error], 'error'), $user_id);
                            exit();
                        }

                        $checkRepo->insertBotUser($user, [
                            'step' => 'coupon',
                            'forward_addr' => $selectBotUser->actions['forward_addr'],
                            'forward_addr2' => $text
                        ]);

                        $this->telegramSendMessage($viewRender->render([], 'coupon.index'), $user_id);

                        break;
                    }
                    case 'coupon':
                    {
                        $request = Coupon::query()->where('code', $text)->first();
                        if (is_null($request)) {
                            $this->telegramSendMessage($this->viewRender->render(['error' => 'The selected coupon is invalid.'], 'error'), $user_id);
                            exit();
                        }
                        if ($request->isUsed()) {
                            $this->telegramSendMessage($this->viewRender->render(['error' => 'The selected coupon is invalid.'], 'error'), $user_id);
                            exit();
                        }

                        $checkRepo->insertBotUser($user, [
                            'step' => 'coupon',
                            'forward_addr' => $selectBotUser->actions['forward_addr'],
                            'forward_addr2' => $selectBotUser->actions['forward_addr2'],
                            'coupon' => $text
                        ]);

                        $selectBotUser = $checkRepo->selectBotUser($user->getId());
                        $this->startMixing($selectBotUser, $user_id, $mainKeyboards, $user);
                        break;
                    }


                }
            }
        }

        return true;
    }


    protected function startMixing(BotUser $selectBotUser, $user_id, $mainKeyboards, $user)
    {

        $data['forward_addr'] = null;
        if (isset($selectBotUser->actions['forward_addr'])) $data['forward_addr'] = $selectBotUser->actions['forward_addr'];

        $data['forward_addr2'] = null;
        if (isset($selectBotUser->actions['forward_addr2'])) $data['forward_addr2'] = $selectBotUser->actions['forward_addr2'];

        $coupon = null;
        if (isset($selectBotUser->actions['coupon'])) {
            $coupon = $selectBotUser->actions['coupon'];
        }

        $order = $this->service->crud($data, $coupon);
        $info = $this->service->info($coupon);

        $this->telegramSendMessage($this->viewRender->render([], 'order.guarantee_letter_heder'), $user_id, $mainKeyboards);
        $this->telegramSendMessage($this->viewRender->render(['order' => $order], 'order.guarantee'), $user_id);
        $this->telegramSendMessage($this->viewRender->render(['info' => $info], 'order.send_to_address'), $user_id);
        $this->telegramSendMessage($this->viewRender->render(['order' => $order], 'order.address'), $user_id);
        $this->telegramSendMessage($this->viewRender->render(['info' => $info], 'order.address_availability'), $user_id);

        Telegram::sendPhoto([
            'chat_id' => $user_id,
            'photo' => InputFile::create('https://jambler.io/libs/qrcode.php?text=bitcoin:' . $order['address']),

        ]);
        $this->checkRepo->insertBotUser($user);
        return true;

    }

    protected function isbotActive(TelegramSetting $setting)
    {
        return $setting->status;

    }

    function telegramSendMessage($text, $user_id, $keyboard = null)
    {
        if (is_null($keyboard))
            Telegram::sendMessage([
                'chat_id' => $user_id,
                'text' => $text,
                'parse_mode' => 'html',
            ]);

        else
            Telegram::sendMessage([
                'chat_id' => $user_id,
                'text' => $text,
                'reply_markup' => $keyboard,
                'parse_mode' => 'html',
            ]);
    }


}
