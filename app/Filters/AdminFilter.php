<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use QueryFilter\QueryFilter;

class AdminFilter extends QueryFilter
{
    public function id($value)
    {
        return $this->builder->where('id', $value);
    }

    public function username($value)
    {
        return $this->builder->where('username', 'like', '%' . $value . '%');
    }

    public function status($value)
    {
        return $this->builder->where('status', $value);
    }

    public function role($value)
    {
        $this->builder->role($value);
    }

    public function created_at($value)
    {
        return $this->builder->where('created_at', $value);
    }

    public function updated_at($value)
    {
        return $this->builder->where('updated_at', $value);
    }


    public function account($value)
    {
        return $this->builder->where('account', 'like', '%' . $value . '%');
    }

    public function options($value)
    {
        return $this->builder->where('options', $value);
    }

}
