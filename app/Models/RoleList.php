<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-05
 * Time: 12:59
 */

namespace App\Models;


class RoleList
{
    const ROLE_ADMIN = 'ADMIN';
    const ROLE_OPERATOR = 'OPERATOR';
    public static function roleList()
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_OPERATOR,
        ];
    }

    public static function roleNames()
    {
        return [
            self::ROLE_ADMIN => 'Администратор',
            self::ROLE_OPERATOR => 'Оператор',
        ];
    }

    public static function permissions()
    {
        return [

            'jambler_api' => [
                'view',
                'save',
            ],
        ];
    }


}
