<?php

namespace App\Models;


use App\Events\TelegramBotConfigureBot;
use App\Traits\UploadImageTrait;

/**
 * App\Models\TelegramSetting
 *
 * @property int                             $id
 * @property string                          $type
 * @property string                          $token
 * @property string|null                     $image
 * @property mixed|null                      $text
 * @property int                             $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel filter($filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TelegramSetting wherePid($value)
 * @property mixed $proxy_bot_id
 */
class TelegramSetting extends BaseModel
{
    use UploadImageTrait;

    protected $table = "telegram_settings";


    protected $fillable = [
        'type', 'status', 'token', 'text', 'image'
    ];

    public static function attributeLabelds()
    {
        return [
            'proxy_id'              => 'Proxy',
            'proxy_bot_id'          => 'Proxy',
            'text'                  => 'Текст приветствия',
            'status'                => 'Статус бота',
            'token'                 => 'Telegram Token',
            'code'                  => 'Code',
        ];
    }


    public function getAttributeLabels()
    {
        return self::attributeLabelds();
    }

    public function logo()
    {
        return $this->fileUrl('image');
    }

    protected static function booted()
    {
        parent::booted();

        self::deleted(
            function ($model) {
                $model->deleteFile('image');
            }
        );
    }

    public function reconfigure()
    {
        event(new TelegramBotConfigureBot($this));
    }


}
