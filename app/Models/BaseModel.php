<?php
/**
 * Created by PhpStorm.
 * User: jasurbek
 * Date: 2020-06-05
 * Time: 20:06
 */

namespace App\Models;


use App\Packages\QueryFilter\src\Traits\FilterableTrait;
use App\View\Interfaces\IFormInputable;
use App\View\Traits\FormInputAttributeLabelTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel query()
 * @mixin \Eloquent
 */
abstract class BaseModel extends Model implements IFormInputable
{
    use FormInputAttributeLabelTrait;
    use FilterableTrait;

    protected static function booted()
    {
        parent::booted();
    }

    public function created_at()
    {
        return $this->created_at->format('d.m.Y H:i');
    }
}
