<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;


/**
 * App\Models\Coupon
 *
 * @property int|null $id
 * @property int $user_id
 * @property string $api_id
 * @property string $code
 * @property Carbon|null $used_at
 * @property Carbon|null $updated_at
 * @property string|null $created_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereUsedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereApiId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereJamblerApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Coupon whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel filter($filter)
 */
class Coupon extends BaseModel
{
    use HasFactory;

    protected $fillable = ['*'];


    public function api()
    {
        return $this->belongsTo(ApiKeys::class, 'api_id');
    }

    public function scopeActive($query)
    {
        return $query->whereNull('used_at');
    }

    public function isUsed()
    {
        return $this->used_at !== null;
    }

    public static function attributeLabels()
    {
        return [
            'api_id' => 'Api',
            'number' => 'Number',
        ];
    }

    public function getAttributeLabels()
    {
        return self::attributeLabels();
    }
}
