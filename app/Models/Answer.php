<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Answer
 *
 * @property int $id
 * @property string $question
 * @property string $answer
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereQuestion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereAnswer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Answer whereUpdatedAt($value)
 * @mixin Model
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel filter($filter)
 */
class Answer extends BaseModel
{
    use HasFactory;

    protected $fillable = ['id', 'question', 'answer', 'status'];

    public static function attributeLabels()
    {
        return [
            'question' => 'Question',
            'answer' => 'Answer',
            'status' => 'Статус',
        ];
    }

    public function getAttributeLabels()
    {
        return self::attributeLabels();
    }

}
