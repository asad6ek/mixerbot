<?php

namespace App\Models;


class StatusList
{
    const ACTIVE = 1;
    const INACTIVE = 0;

    public static function activeInactive()
    {
        return [
            self::ACTIVE => 'Активный',
            self::INACTIVE => 'Не активный',
        ];
    }

    public static function classList()
    {
        return [
            self::ACTIVE => 'success',
            self::INACTIVE => 'danger',
        ];
    }
}
