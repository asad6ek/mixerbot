<?php

namespace App\Models;

use App\Casts\JsonCast;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BotUser
 *
 * @property int $id
 * @property int $chat_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property mixed $actions
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel filter($filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser whereChatId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BotUser whereId($value)
 * @mixin \Eloquent
 */
class BotUser extends BaseModel
{
    protected $fillable = [
        'chat_id', 'first_name', 'last_name'
    ];

    protected $casts = [
        'actions' => 'array',
    ];


    public function setFirstNameAttribute($value)
    {
        $this->attributes['first_name'] = strtolower($value);
    }

}
