<?php

namespace App\Models;

use App\Traits\ModelListTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\ApiKeys
 *
 * @property int         $id
 * @property string      $api_key
 * @property bool      $default
 * @property string|null $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys whereApiKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ApiKeys whereUpdatedAt($value)
 * @mixin Model
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseModel filter($filter)
 */
class ApiKeys extends BaseModel
{
    use HasFactory;
    use SoftDeletes;
    use ModelListTrait;

    protected $fillable = ['id', 'api_key', 'name'];


    public function coupons()
    {
        return $this->hasMany(Coupon::class, 'api_id');
    }

    public static function attributeLabels()
    {
        return [
            'name' => 'Название',
        ];
    }

    public function getAttributeLabels()
    {
        return self::attributeLabels();
    }

    public function name()
    {
        return $this->api_key . ' ('. $this->name.')';
    }

}
