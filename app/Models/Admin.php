<?php

namespace App\Models;

use App\Filters\AdminFilter;
use App\View\Interfaces\IFormInputable;
use App\View\Traits\FormInputAttributeLabelTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;


/**
 * App\Models\Admin
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereUsername($value)
 * @mixin \Eloquent
 * @property int $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereStatus($value)
 * @property string|null $deleted_at
 * @property-read mixed $role
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin filter($filter)
 * @property string|null $account
 * @property mixed|null $options
 * @property int $main_admin
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereAccount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereAmountPerProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereMainAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereOptions($value)
 * @property-read int|null $cities_count
 * @property-read mixed $city_list
 * @property-read mixed $user_list
 * @property-read int|null $users_count
 * @property string|null $enter_token
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Admin whereEnterToken($value)
 */
class Admin extends Authenticatable
{
    use Notifiable;


    protected $queryFilterClass = AdminFilter::class;

    protected $guard_name = 'web';

    protected $fillable = ['username', 'status', 'account'];

    protected static function booted()
    {
        parent::booted();
    }


    public static function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'password' => 'Пароль',
            'role' => 'Роль',
            'status' => 'Статус',
        ];
    }

    public function getAttributeLabels()
    {
        return self::attributeLabels();
    }


    public function isMainAdmin()
    {
        return (int)$this->main_admin === 1;
    }


    public function userNames()
    {
        $ids = $this->users->pluck('child_id')->toArray();
        return Admin::whereIn('id', $ids)->pluck('username')->implode(', ');
    }

    public function userRole()
    {
        return Arr::get(RoleList::roleNames(), $this->role);
    }
}
