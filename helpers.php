<?php

use Illuminate\Support\Facades\Route;


function isActiveRoute($route, $strict = false, $className = 'active')
{
    $name = Route::currentRouteName();
    if (!$strict) {
        return \Illuminate\Support\Str::is($route . '*', $name) ? $className : '';
    }
    return $name === $route ? $className : '';
}

function alertMessage($type, $message)
{
    session()->flash('alert-message', $message);
    session()->flash('alert-type', $type);
}

function modalMessage($type, $message)
{
    session()->flash('modal-message', $message);
    session()->flash('modal-type', $type);
}

function resourceWithUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');
    Route::resource($url, $controller)->names([
        'index' => $url . '.index',
        'create' => $url . '.create',
        'store' => $url . '.store',
        'edit' => $url . '.edit',
        'update' => $url . '.update',
        'destroy' => $url . '.delete',
    ]);
}

function apiUpload($url, $controller)
{
    Route::post($url . '/process', $controller . '@processImage');
    Route::delete($url . '/revert', $controller . '@revertImage');
    Route::get($url . '/load/{path}', $controller . '@loadImage');
    Route::post($url . '/{id}/process', $controller . '@processImage');
    Route::delete($url . '/{id}/revert', $controller . '@revertImage');
    Route::get($url . '/{id}/load/{path}', $controller . '@loadImage');

}

function resource($url, $controller, $name = null)
{
    Route::resource($url, $controller)->names([
        'index' => ($name ?: $url) . '.index',
        'create' => ($name ?: $url) . '.create',
        'store' => ($name ?: $url) . '.store',
        'edit' => ($name ?: $url) . '.edit',
        'update' => ($name ?: $url) . '.update',
        'destroy' => ($name ?: $url) . '.delete',
        'show' => ($name ?: $url) . '.show',
    ]);
}

if (!function_exists('ip')) {

    function ip()
    {
        return Request::getClientIp();
    }
}

if (!function_exists('menus')) {
    function menus()
    {
        return \App\Models\Page::query()
            ->where('status', 1)
            ->where('isShownMenu', 1)
            ->get();
    }
}
if (!function_exists('news')) {
    function news()
    {
        return \App\Models\ShopNews::query()
            ->where('status', 1)
            ->get();
    }
}

if (!function_exists('isActivePage')) {
    function isActivePage($url, $className = 'active')
    {

        return Request::path() === $url ? $className : '';
    }
}

if (!function_exists('shop')) {
    function shop()
    {
        return optional(Shop::shop());
    }
}
