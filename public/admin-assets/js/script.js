"use strict";

(function (window, $) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function deleteWithConfirmation(event) {
        event.preventDefault();

        const target = event.target.closest('a');

        console.log(target, target.getAttribute('href'));
        swal({
            title: 'Вы уверены?',
            text: "Данные безвозвратно удалятся.",
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success btn-fill',
            cancelButtonClass: 'btn btn-danger btn-fill',
            confirmButtonText: 'Да',
            cancelButtonText: 'Отмена',
            buttonsStyling: false
        }).then(function (data) {
            if (data.value === true) {
                $.ajax({
                    method: 'DELETE',
                    url: target.getAttribute('href'),
                }).done(function (data) {
                    if (data['redirect'] || data['reload']) {
                        location.reload();
                    }

                }).fail(function () {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                });

            }


        });
    }

    function confirmWithText(event) {
        event.preventDefault();

        const target = event.target.closest('a');
        console.log(target, target.getAttribute('href'));

        swal({
            title: 'Info',
            html: '<div class="form-group">' +
                '<textarea id="input-field" type="text" class="form-control" ></textarea>' +
                '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success btn-fill',
            cancelButtonClass: 'btn btn-danger btn-fill',
            buttonsStyling: false
        }).then(function (data) {
            if (data.value === true) {
                const text = $('#input-field').val();
                $.ajax({
                    method: 'POST',
                    url: target.getAttribute('href'),
                    data: {
                        text,
                    },
                }).done(function (data) {
                    location.reload();
                }).fail(function () {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong!',
                    })
                });

            }

        });


    }

    window.deleteWithConfirmation = deleteWithConfirmation;
    window.confirmWithText = confirmWithText;

    function readImageAndLoad(input, img) {
        $(input).on('change', function () {
            openFile(this, img);
        });
    }

    var openFile = function (input, img) {

        var reader = new FileReader();
        reader.onload = function () {
            var dataURL = reader.result;
            $(img).attr('src', dataURL);
            $(input).trigger('image:uploaded');
        };

        reader.readAsDataURL(input.files[0]);
    };


    function imagePreview(placeholder, input, img) {
        if (!img) {
            img = input;
            input = placeholder;

            $(img).on('click', function (e) {
                $(input).click();
            });
            readImageAndLoad(input, img);
            return;
        }

        $(placeholder).on('click', function (e) {
            $(input).click();
        });
        readImageAndLoad(input, img);

    }

    window.imagePreview = imagePreview;
    window.readImageAndLoad = readImageAndLoad;

})(window, jQuery);
