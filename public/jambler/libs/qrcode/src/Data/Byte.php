<?php


namespace chillerlan\QRCode\Data;

use chillerlan\QRCode\QRCode;

/**
 * Byte mode, ISO-8859-1 or UTF-8
 */
class Byte extends QRDataAbstract{

	/**
	 * @inheritdoc
	 */
	protected $datamode = QRCode::DATA_BYTE;

	/**
	 * @inheritdoc
	 */
	protected $lengthBits = [8, 16, 16];

	/**
	 * @inheritdoc
	 */
	protected function write($data){
		$i = 0;

		while($i < $this->strlen){
			$this->bitBuffer->put(ord($data[$i]), 8);
			$i++;
		}

	}

}
