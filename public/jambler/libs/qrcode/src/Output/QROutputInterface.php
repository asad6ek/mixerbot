<?php

namespace chillerlan\QRCode\Output;

/**
 * Converts the data matrix into readable output
 */
interface QROutputInterface{

	/**
	 * @return mixed
	 */
	public function dump();

}
