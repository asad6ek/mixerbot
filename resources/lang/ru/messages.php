<?php


return [
    'created_successfully' => ':Main добавлен',
    'updated_successfully' => ':Main изменён',
    'deleted_successfully' => ':Main удалён',
    'no_permission' => 'Нет разрешения',
    'not_found' => 'Не найден',
    'save' => 'Сохранить',
    'available_products_not_found' => 'Товары на продажу отсутствуют',
    'nothing_found' => 'Данные отсутствуют',
    'privilege_view' => 'Просмотр',
    'privilege_save' => 'Редактирование',
];
