@extends('main.layout.layout')
@section('content')
    @isset($info['err'])
        {{ $info['err'] }}
    @else
        <div class="text-center">
            <h1>Bitcoin Mixer&nbsp;2.0</h1>
            <p class="big">Use your coupon</p>
        </div>


        <section>
            <div class="container">
                <div class="how"></div>
                <div class="row">
                    <div class="col-12 text-center">
                        <form action="{{ route('coupon.use') }}" method="post">
                            @csrf
                            <input type="text" class="form-control" name="coupon" placeholder="Enter coupon"
                                   required="">
                            <div class="row mt-4">
                                <div class="col-12">
                                    <button type="submit" name="submit_mix" class="btn btn-jam">Mix My Coins</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    @endisset

@endsection
