<?php
/**
 * @var $user \App\User
 */

?>
<div>
    <b>Группа клиентов: </b>{{ $user->clientType() }} <br>
    <x-client.balance-info :user="$user"/>
    <div>
        <b>Сумма покупок:</b>
        @forelse($orderAmounts as $currency =>  $orderAmount)
            <br>
            <span>{{ $currencyName($currency) }}</span>: {{$orderAmount}}
        @empty
            0
        @endforelse
    </div>
    <b>Перезаклад:</b> {{ $numPerezaklad }}<br>
</div>
