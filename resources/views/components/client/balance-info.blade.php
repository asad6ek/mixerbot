<div class="user-balance-list">
	<b class="user-balance-header">Бонусный счёт:</b>
	@forelse($balances as $balance)
		<br>
        <span>{{ $balance->currencyName() }}</span>: {{ $balance->balance }}
	@empty
		0
	@endforelse
</div>
