<?php
/**
 * @var $attributes \Illuminate\View\ComponentAttributeBag
 */


?>
<x-html.tag type="div" :options="$divOptions">
	<x-html.tag type="label" :options="$labelOptions"/>
	<input
			value="{{ $attributes->get('value', $value)  }}"
			name="{{ $attributes->get('name', $attribute) }}"
			type="{{ $attributes->get('type', 'text') }}"
			{!! \App\View\AttributeLister::merge($attributes, $options) !!}
	/>
</x-html.tag>
