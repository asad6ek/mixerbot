<x-html.tag type="div" :options="$divOptions">
	<x-html.tag type="label" :options="$labelOptions"/>
	<textarea name="{{ $attributes->get('name', $attribute) }}"
			{!! \App\View\AttributeLister::merge($attributes, $options) !!} >{{ $attributes->get('value', $value)  }}</textarea>
</x-html.tag>
