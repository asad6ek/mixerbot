@if($stats)
    <div class="card ">
        <div class="card-header">
            <h4 class="card-title">Статистика поступлений</h4>
        </div>
        <div class="card-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr class="success">
                                <td><b></b></td>
                                @foreach($stats as $stat)
                                    <td><b>{{ $stat->name() }}</b></td>
                                @endforeach
                            </tr>
                            <tr>
                                <td><b>Оплачено</b></td>
                                @foreach($stats as $stat)
                                    <td>
                                        {{ $stat->paidCount() }} заказов<br>
                                        Сумма: @formatDecimal( $stat->paidSum() ) {{ $stat->currency() }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td><b>Нет в наличии</b></td>
                                @foreach($stats as $stat)
                                    <td>
                                        {{ $stat->notAvailableCount() }} заказов<br>
                                        Сумма: @formatDecimal( $stat->notAvailableSum() ) {{ $stat->currency() }}
                                    </td>
                                @endforeach
                            </tr>
                            <tr>
                                <td><b>Всего</b></td>
                                @foreach($stats as $stat)
                                    <td>
                                        {{ $stat->overAllCount() }} заказов<br>
                                        Сумма: @formatDecimal( $stat->overAllSum() ) {{ $stat->currency() }}
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
