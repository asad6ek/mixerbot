@foreach($messages as $type => $messageList)

	@if(is_array($messageList))

		@foreach($messages as  $message)
			<x-alert :message="$message" :type="$type"/>
		@endforeach

	@else
		<x-alert :message="$messageList" :type="$type"/>
	@endif

@endforeach
