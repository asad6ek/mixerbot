@if((int)$status  === \App\Models\Order::STATUS_WAITING)
	<span class="label label-danger">в ожидании</span>
@elseif((int)$status  === \App\Models\Order::STATUS_PAID)
	<span class="label label-success">Оплачен</span>
@elseif((int)$status  === \App\Models\Order::STATUS_NOT_AVAILABLE_ADDRESS)
	<span class="label label-warning">нет в наличии</span>
@elseif((int)$status  === \App\Models\Order::STATUS_NOT_PAID_FULLY)
	<span class="label label-danger">в ожидании</span>
@elseif((int)$status  === \App\Models\Order::STATUS_PAID_LATE)
	<span class="label label-danger">в ожидании</span>
@endif
