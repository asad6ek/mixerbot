@foreach($messages as $message)
<div class="alert alert-{{ $type }}">
	{{ $message }}
</div>
@endforeach
