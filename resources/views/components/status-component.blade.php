<x-html.tag type="label" class="label label-{{ $statusClass }}">
    {{ $statusText }}
</x-html.tag>
