<div class="row">
	<div class="col-md-12">
		<x-errors :errors="$errors" />
		<div class="form-group">
			<label for="">{{ __('validation.attributes.old_password') }}</label>
			<x-inputs.input name="old-password" value="" type="password" />
		</div>

		<div class="form-group">
			<label for="">{{ __('validation.attributes.new_password') }}</label>
			<x-inputs.input name="new_password" value="" type="password" />
		</div>

		<div class="form-group">
			<label for="">{{ __('validation.attributes.password_confirmation') }}</label>
			<x-inputs.input name="new_password_confirmation" value="" type="password" />
		</div>

		<div class="text-center">
			<button class="btn-success btn">{{ __('messages.save') }}</button>
		</div>
	</div>
</div>
