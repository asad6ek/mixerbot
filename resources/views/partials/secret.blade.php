<div class="row">
    <div class="col-md-12">
        <x-errors :errors="$errors"/>
        <div class="form-group">
            <label for=""> Имя пользователя </label>
            <x-inputs.input name="username" :value="old('username')" type="text"/>
        </div>

        <div class="form-group">
            <label for=""> Секретные слова </label>
            <x-inputs.input name="secret" value="" type="text"/>
        </div>

        <div class="text-center">
            <button class="btn-success btn">Отправить</button>
        </div>
    </div>
</div>
