
<div class="row">
	<div class="col-md-12">
		<x-errors :errors="$errors" />
            @csrf
            <div class="form-group field-signupform-password required">
                <label class="control-label" for="signupform-password">Введите новый пароль</label>
                <input type="password" id="signupform-password" class="form-control" name="password"
                       aria-required="true">
            </div>
            <div class="form-group field-signupform-compassword required">
                <label class="control-label" for="signupform-compassword">Подтверждение нового пароль</label>
                <input type="password" id="signupform-compassword" class="form-control" name="password_confirmation"
                       aria-required="true">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary" name="signup-button">Изменить</button>
            </div>
	</div>
</div>
