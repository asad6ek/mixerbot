<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8"/>
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('admin-assets') }}/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('admin-assets') }}/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

	<title>@yield('title','Rcbot')</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
	<meta name="viewport" content="width=device-width"/>
	<meta name="csrf-token" content="{{ csrf_token() }}">



	<!-- Bootstrap core CSS     -->
	<link href="{{ asset('admin-assets') }}/css/bootstrap.min.css" rel="stylesheet"/>

	<!--  Paper Dashboard core CSS    -->
	<link href="{{ asset('admin-assets') }}/css/paper-dashboard.css" rel="stylesheet"/>


	<!--  CSS for Demo Purpose, don't include it in your project     -->
	<link href="{{ asset('admin-assets') }}/css/demo.css" rel="stylesheet"/>


	<!--  Fonts and icons     -->
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href="{{ asset('admin-assets') }}/css/themify-icons.css" rel="stylesheet">
</head>

<body>
@stack('head_script')

	@yield('base-content')

<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
<script src="{{ asset('admin-assets') }}/js/jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('admin-assets') }}/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="{{ asset('admin-assets') }}/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('admin-assets') }}/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Forms Validations Plugin -->
<script src="{{ asset('admin-assets') }}/js/jquery.validate.min.js"></script>

<!-- Promise Library for SweetAlert2 working on IE -->
<script src="{{ asset('admin-assets') }}/js/es6-promise-auto.min.js"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('admin-assets') }}/js/moment.min.js"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="{{ asset('admin-assets') }}/js/bootstrap-datetimepicker.js"></script>

<!--  Select Picker Plugin -->
<script src="{{ asset('admin-assets') }}/js/bootstrap-selectpicker.js"></script>

<!--  Switch and Tags Input Plugins -->
<script src="{{ asset('admin-assets') }}/js/bootstrap-switch-tags.js"></script>

<!-- Circle Percentage-chart -->
<script src="{{ asset('admin-assets') }}/js/jquery.easypiechart.min.js"></script>

<!--  Charts Plugin -->
<script src="{{ asset('admin-assets') }}/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('admin-assets') }}/js/bootstrap-notify.js"></script>

<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('admin-assets') }}/js/sweetalert2.js"></script>

<!-- Vector Map plugin -->
<script src="{{ asset('admin-assets') }}/js/jquery-jvectormap.js"></script>


<!-- Wizard Plugin    -->
<script src="{{ asset('admin-assets') }}/js/jquery.bootstrap.wizard.min.js"></script>

<!--  Bootstrap Table Plugin    -->
<script src="{{ asset('admin-assets') }}/js/bootstrap-table.js"></script>

<!--  Plugin for DataTables.net  -->
<script src="{{ asset('admin-assets') }}/js/jquery.datatables.js"></script>

<!--  Full Calendar Plugin    -->
<script src="{{ asset('admin-assets') }}/js/fullcalendar.min.js"></script>

<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
<script src="{{ asset('admin-assets') }}/js/paper-dashboard.js?v=2"></script>

<!-- Paper Dashboard PRO DEMO methods, don't include it in your project! -->
<script src="{{ asset('admin-assets') }}/js/demo.js"></script>
<script src="{{ asset('js') }}/app.js?v=2"></script>
<script src="{{ asset('admin-assets') }}/js/script.js?v=4"></script>

@stack('scripts')

@stack('ckeditor_scripts')
</body>


</html>

