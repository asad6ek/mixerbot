@extends('template.layout.base-layout')
@section('base-content')
	<div class="wrapper">
		@yield('sidebar', View::make('template.layout.sidebar'))

		<div class="main-panel">
			@yield('navbar', View::make('template.layout.navbar'))
			<div class="content">
				<div class="container-fluid">
					<x-session-alert/>
				</div>
				@yield('content-main')
			</div>


	@include('template.layout.footer')
@endsection

