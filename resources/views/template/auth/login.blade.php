@extends('template.layout.base-layout')

@section('base-content')
	<div class="wrapper wrapper-full-page">
		<div class="full-page login-page" data-color=""
		     data-image="/{{'admin-assets'}}/img/background/background-2.jpg">
			<!--   you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" -->
			<div class="content">
				<div class="container">
					<div class="row">
						<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
							<form method="POST">
								@csrf
								<x-errors :errors="$errors" />
								<div class="card" data-background="color" data-color="blue">
									<div class="card-header">
										<h3 class="card-title">Авторизоваться</h3>
									</div>
									<div class="card-content">
										<div class="form-group">
											<label>Логин</label>
											<input type="text" placeholder="Логин" name="username"
											       class="form-control input-no-border" value="{{old('username')}}">
										</div>
										<div class="form-group">
											<label>Пароль</label>
											<input type="password" placeholder="Пароль" name="password"
											       class="form-control input-no-border">
										</div>
									</div>
									<div class="card-footer text-center">
										<button type="submit" class="btn btn-fill btn-wd ">Войти</button>
									</div>
								</div>

							</form>
						</div>
					</div>
				</div>
			</div>

			<footer class="footer footer-transparent">
				<div class="container">
				</div>
			</footer>
		</div>
	</div>
@endsection

@push('scripts')
	<script>
        $().ready(function () {
            demo.checkFullPageBackgroundImage();

            setTimeout(function () {
                // after 1000 ms we add the class animated to the login/register card
                $('.card').removeClass('card-hidden');
            }, 700)
        });
	</script>
@endpush
