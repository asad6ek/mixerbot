<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-minimize">
			<button id="minimizeSidebar" class="btn btn-fill btn-icon"><i class="ti-more-alt"></i></button>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar bar1"></span>
				<span class="icon-bar bar2"></span>
				<span class="icon-bar bar3"></span>
			</button>
			<a class="navbar-brand" href="#Dashboard">
				@yield('page-title')
			</a>
		</div>
		<div class="collapse navbar-collapse">


			<ul class="nav navbar-nav navbar-right">
{{--				<li class="{{ isActiveRoute('admin.news') }}">--}}
{{--					<a href="{{ route('admin.news') }}">--}}
{{--						<i class="ti-announcement"></i>--}}
{{--						<p>Новости</p>--}}
{{--					</a>--}}
{{--				</li>--}}
{{--				<li class="{{ isActiveRoute('admin.faq') }}">--}}
{{--					<a href="{{ route('admin.faq') }}">--}}
{{--						<i class="ti-info-alt"></i>--}}
{{--						<p>FAQ</p>--}}
{{--					</a>--}}
{{--				</li>--}}
{{--				<li class="{{ isActiveRoute('admin.settings') }}">--}}
{{--					<a href="{{ route('admin.settings') }}" class="btn-rotate">--}}
{{--						<i class="ti-settings"></i>--}}
{{--						Настройки--}}
{{--					</a>--}}
{{--				</li>--}}
{{--				<li>--}}
{{--					<a href="{{ route('admin.auth.logout') }}">--}}
{{--						<i class="ti-back-right"></i>--}}
{{--						<p>Выход</p>--}}
{{--					</a>--}}
{{--				</li>--}}

			</ul>
		</div>
	</div>
</nav>
