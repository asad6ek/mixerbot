<div class="sidebar" data-background-color="brown" data-active-color="danger">
    <!--
Tip 1: you can change the color of the sidebar's background using: data-background-color="white | brown"
        Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
    -->
    <div class="logo">
        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-mini">
            M
        </a>

        <a href="{{ route('admin.dashboard') }}" class="simple-text logo-normal">
            Mixer
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="info">
                <div class="photo">
                    <img src="{{asset('admin-assets')}}/img/faces/face-2.jpg"/>
                </div>

                <a href="#">
	                        <span>
								{{ optional(auth('admin')->user())->username }}
							</span>
                </a>

            </div>
        </div>
        <ul class="nav">
            {{--            {{isActiveRoute('admin.dashboard', true)}}--}}
            <li class="">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="ti-bar-chart-alt"></i>
                    <p>Главная</p>
                </a>
            </li>

            {{--			@can('viewAny',\App\Models\ApiKeys::class)--}}
            <li class="
{{--{{isActiveRoute('admin.jambler_api')}}--}}
                ">
                <a href="{{route('admin.jambler_api.index')}}">
                    <i class="ti-map"></i>
                    <p>Api keys</p>
                </a>
            </li>
            <li class="
                ">
                <a href="{{route('admin.coupon.index')}}">
                    <i class="ti-map"></i>
                    <p>Coupon</p>
                </a>
            </li>

            <li class="
">
                <a href="{{ route('admin.answer.index') }}">
                    <i class="ti-book"></i>
                    <p>Answers</p>
                </a>
            </li>

            <li class="
">
                <a href="{{ route('admin.telegram') }}">
                    <i class="ti-text"></i>
                    <p>Telegram</p>
                </a>
            </li>


        </ul>
    </div>
</div>
