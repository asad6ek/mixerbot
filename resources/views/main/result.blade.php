<?php
/**
 * @var $order \App\DDD\Jambler\Response\Order
 * @var $info  \App\DDD\Jambler\Response\Info
 */
?>
@extends('main.layout.layout')

@section('content')

    <section class="send_bitcoin mb-5">

        <div class="text-center mb-4">

            <form class="mt-4" action="{{route('main.guarantee')}}" method="POST">
                @csrf
                <input type="hidden" name="text" value="{{$order['guarantee']}}">
                <button type="submit" class="btn btn-yellow">
                    Скачать гарантийное письмо
                </button>
            </form>

        </div>

        <div class="blockin">

            <div class="bg_style_shadow">

                <div class="inside__send_bitcoin">


                    <div class="left__send_bitcoin">
                        <img src="jambler/libs/qrcode.php?text=bitcoin:{{$order['address']}}">
                    </div>



                    <div class="right__send_bitcoin">
                        <h4>
                            отправьте Биткоины на этот адрес
                            <span class="min_text">
											(min {{$info['min_amount']}}, max {{$info['max_amount']}} BTC)
										</span>
                        </h4>
                        <label for="bitcoin_address" class="label_copy">
                            <input type="text" id="bitcoin_address" value="{{$order['address']}}" name="bitcoin_address" class="input_copy" readonly>
                            <div class="icon_input_copy">
                                <span class="icon icon-copy"></span>
                            </div>
                        </label>
                        <div class="info">
                            Ваша сессия очистки биткойнов действительна в течение {{$info['withdraw_max_timeout']}} часов. Отправьте свои монеты на адрес депозита в течение этого срока. Мы запланируем выплату ваших новых чистых монет, как только ваш депозит получит 1 подтверждение.
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <script>
            $(document).ready(function () {
                $('#loader').removeClass('d-none');
                var req = function () {
                    $.get('{{env('MAIN_SITE')}}/libs/blockchain-info.php?address= {{$order['address']}}').done(resp => {
                        resp = JSON.parse(resp);
                        if (resp.err) {
                            $('#blockchain_error').text(resp.err);
                            $('#blockchain_error').removeClass('d-none');
                            $('#loader').hide();
                        } else if (+resp.total_received === 0) {
                            setTimeout(req, 20000);
                        } else {
                            $('#loader').hide();
                            $('#blockchain_result span').text(
                                Math.round((resp.total_received / 100000000) * 100000) / 100000
                            );
                            $('#blockchain_result').removeClass('d-none');
                        }
                    });
                }
                req();
            });
        </script>
    </section>

    <section class="calc">

        <div class="blockin">

            <div class="bg_style_shadow">

                <div class="inside__calc">

                    <h4>
                        Калькулятор стоимости и комиссии
                    </h4>
                    <div class="row">

                        <div class="col-lg-6">
                            <div for="bitcoin_address" class="label_copy">
                                <label for="send" class="label_copy">
                                    Сумма, которую Вы хотите отправить
                                </label>
                                <input type="number" class="input_copy" name="you_send" value="" min="{{$info['min_amount']}}" max="{{$info['max_amount']}}" step=".001" required>

                                <div class="icon_input_copy">
                                    <span class="icon icon-copy"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div for="bitcoin_address" class="label_copy">
                                <label for="get" class="label_copy">
                                    Сумма, которую Вы получите после списания с комиссии
                                </label>
                                <b id="you_receive" class="input_copy">-{{$info['mixer_fix_fee']}}</b>
                                <div class="icon_input_copy">
                                    <span class="icon icon-copy"></span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>

{{--
{{--                </div>--}}
{{--                <div class="mt-4 text-center">--}}
{{--                    <a href="{{route('main.index')}}" class="btn btn-jam btn-jam-white">Mix More Coins</a>--}}
{{--                </div>--}}
{{--                <div class="text-center mt-5">--}}
{{--                    <p>Your money will be returned in different parts to addresses specified above</p>--}}
{{--                    <p>--}}
{{--                        Time of return will be {{$info['withdraw_max_timeout']}} hours--}}
{{--                        <span class="ml-2 mr-2">|</span>--}}
{{--                        Service fee: {{$info['mixer_fee_pct']}}% + {{$info['mixer_fix_fee']}} BTC--}}
{{--                        <span class="ml-2 mr-2">|</span>--}}
{{--                        A generated address is valid for {{$info['order_lifetime']}} days (See <a href="#">FAQ</a>)--}}
{{--                    </p>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--                        <div class="mt-5">--}}
{{--                            <a name="calculator"></a>--}}
{{--                            <h2 class="text-center">Exact Value & Fee Calculator</h2>--}}

{{--                            <form class="mt-4" action="#" method="POST">--}}

{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-2 col-form-label text-right">You send:</label>--}}
{{--                                    <div class="col-3">--}}
{{--                                        <div class="input-group">--}}
{{--                                            <input type="number" class="form-control text-right" name="you_send" value="" min="{{$info['min_amount']}}" max="{{$info['max_amount']}}" step=".001" required>--}}
{{--                                            <div class="input-group-append">--}}
{{--                                                <span class="input-group-text">BTC</span>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <p class="col-7 text-14">There is a field for entering a sum which a user wants to transfer, the same as above the QR code. It is also a link to the blockchain.info service</p>--}}
{{--                                </div>--}}

{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-2 col-form-label text-right">You receive:</label>--}}
{{--                                    <div class="col-3 col-form-label text-right">--}}
{{--                                        <b id="you_receive"></b> BTC--}}
{{--                                    </div>--}}
{{--                                    <p class="col-7 text-14">Your commission is not more than {{$info['mixer_fee_pct']}}% and {{$info['mixer_fix_fee']}} BTC – the BTC value is calculated, the commission value is taken from a letter of guarantee</p>--}}
{{--                                </div>--}}


{{--                                <div id="calculate_btn" class="col-12 text-center">--}}
{{--                                    <button type="submit" name="submit_calculator" class="btn btn-jam btn-jam-white">Calculate</button>--}}
{{--                                </div>--}}

{{--                            </form>--}}

{{--                        </div>--}}
{{--    </section>--}}
@endsection
