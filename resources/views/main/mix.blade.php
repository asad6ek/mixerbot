<?php
/**
 * @var $order \App\DDD\Jambler\Response\Order
 * @var $info  \App\DDD\Jambler\Response\Info
 */
?>
@extends('main.layout.layout')

@section('content')

    <section>
        <div class="container">

            <div class="text-center">
                <h1>Bitcoin Mixer&nbsp;2.0</h1>
                <p class="big mt-4 d-none d-sm-block">Get cleanest coins from<br>European, Asian and North American<br>cryptocurrency
                    stock exchanges</p>
                <p class="mt-5">Enter your Bitcoin forward to address below:</p>
            </div>
            <div class="row mt-3">
                <div class="m-auto col-12 col-sm-9 col-md-7 col-lg-5 col-xl-4 d-inline-block text-center">
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            <div class="color-red text-left">
                                {{ $error }}
                            </div>
                        @endforeach
                    @else
                        <div class="color-red text-left">
                            {{ $error }}
                        </div>
                    @endif

                    <form method="post" action="{{route('main.store')}}">
                        @csrf
                        <input type="text" class="form-control" name="forward_addr"
                               placeholder="Enter first address (mandatory)" required>

                        <input type="text" class="form-control mt-2" name="forward_addr2"
                               placeholder="Enter second address (optional)">
                        <input type="text" class="form-control" name="coupon"
                               placeholder="Enter coupon">

                        <div class="row mt-4">
                            <div class="col-12">
                                <button type="submit" name="submit_mix" class="btn btn-jam">Mix My Coins</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="text-center mt-5">
                <p>Your money will be returned in different parts to addresses specified above</p>
                <p>
                    Time of return will be {{$info['withdraw_max_timeout']}} hours
                    <span class="ml-2 mr-2">|</span>
                    Service fee: {{$info['mixer_fee_pct']}}% + {{$info['mixer_fix_fee']}} BTC
                    <span class="ml-2 mr-2">|</span>
                    A generated address is valid for {{$info['order_lifetime']}} days (See <a href="#">FAQ</a>)
                </p>
            </div>
        </div>
    </section>

@endsection
