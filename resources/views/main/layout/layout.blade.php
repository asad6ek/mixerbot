<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8">
    <title>{{$info['mixer_name'] ?? 'Error'}}</title>
    <meta name="description" content="{{$info['mixer_name'] ?? 'Error'}}"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Favicons -->
    <link rel="icon" href="{{asset('images/favicon/cropped-32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('images/favicon/cropped-192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/favicon/cropped-32x32.png')}}">
    <meta name="msapplication-TileImage" content="images/favicon/cropped-32x32.png">

    <!-- Styles start -->
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <!-- Styles end -->

    <!-- Fonts start -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;700&display=swap" rel="stylesheet">
    <!-- Fonts end -->
    <script src="/jambler/libs/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#calculate_btn').hide();
            $('input[name=you_send]').on('keyup mouseup', e => {
                var mixer_fee_pct =  {{$info['mixer_fee_pct']??1}};
                var mixer_fix_fee = {{$info['mixer_fix_fee']??1}};
                var you_send = $(e.target).val();
                $('#you_receive').text(Math.round((you_send - you_send * mixer_fee_pct / 100 - mixer_fix_fee) * 100000) / 100000);
            });
        });
    </script>

</head>

<body>

<div class="wrapper">
    <section class="top-section">

    @include('main.layout.top')

@yield('content')
    </section>
@include('main.layout.footer')
</div>
<script src="{{asset('libs/jquery-3.4.1.min.js')}}"></script>

<script src="{{asset('js/scripts.min.js?ver=0.2')}}"></script>

</body>

</html>
