    <header>
        <div class="header">
            <div class="blockin">
                <div class="inside_header">
                    <div class="left-header">
                        <a href="/" class="logo">
                            <img src="images/logo.svg" alt="">
                        </a>
                    </div>
                    <div class="right-header">
                        <nav class="menu-header">
                            <div class="burger collapsed">
                                <div class="wrap_inside--burger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <span>МЕНЮ</span>
                            </div>
                            <div class="list-menu-wrap">
                                <div class="close_menu">&times;</div>
                                <ul class="list-menu list-header-menu">
                                    <li class="current">
                                        <a class="scroll" href="#how_it_works">
                                            Как это работает
                                        </a>
                                    </li>
                                    <li>
                                        <a class="scroll" href="#advantages">
                                            Наши преимущества
                                        </a>
                                    </li>
                                    <li>
                                        <a class="scroll" href="#faq">
                                            FAQ
                                        </a>
                                    </li>
                                    <li>
                                        <a class="scroll" href="#contacts">
                                            Контакты
                                        </a>
                                    </li>
                                </ul>
{{--                                <div class="languages">--}}
{{--                                    <div class="current-lang">--}}
{{--                                        RU--}}
{{--                                    </div>--}}
{{--                                    <ul class="list-langs">--}}
{{--                                        <li>--}}
{{--                                            <a href="#">--}}
{{--                                                ENG--}}
{{--                                            </a>--}}
{{--                                        </li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                            </div>
                        </nav>
                        <a href="#" class="btn">
                            <span class="icon icon-telegram"></span>
                            <span>
										Telegram Bot
									</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>


