<footer class="footer">
    <div class="blockin">
        <div class="inside--footer">
            <div class="row">
                <div class="col-lg-3">
                    <div class="footer__inside_col">
                        <a href="index.html" class="logo">
                            <img src="images/logo_footer.svg" alt="">
                        </a>
                        <div class="copyright">
                            © 2020, Whitener.io
                        </div>
                        <ul class="social_list">
                            <li class="social_item">
                                <a href="#" class="social_link">
                                    <img src="images/icons/social_reddit.svg">
                                </a>
                            </li>
                            <li class="social_item">
                                <a href="#" class="social_link">
                                    <img src="images/icons/social-mail-filled.svg">
                                </a>
                            </li>
                            <li class="social_item">
                                <a href="#" class="social_link">
                                    <img src="images/icons/social-bitcoin.svg">
                                </a>
                            </li>
                            <li class="social_item">
                                <a href="#" class="social_link">
                                    <img src="images/icons/social-telegram.svg">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <ul class="list-menu list-menu-footer">
                        <li>
                            <a href="#">
                                Как это работает
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Наши преимущества
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                FAQ
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Контакты
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-5">
                    <div class="footer__right_title">
                        Отпечаток PGP Jambler.io:
                    </div>
                    <a href="#" class="footer__btn">
								<span class="contact__wrap_icon">
									<img src="images/icons/key.svg" class="contact__icon" alt="">
									<span class="contact_text">
										B8A5 CFCA F63F F2D8 384A 6B12 D3B2 8095 6F0E 7CAF
									</span>
								</span>
                    </a>
                    <div class="footer__right_text">
                        <p>
                            Перейдите по ссылке, чтобы загрузить открытый ключ для проверки гарантийных писем, предоставленных платформой.
                        </p>
                        <p>
                            * Для получения дополнительной информации о том, как это работает, см. FAQ.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
