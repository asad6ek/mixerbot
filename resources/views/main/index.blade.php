@extends('main.layout.layout')
@section('content')
    @isset($info['err'])
        {{ $info['err'] }}
    @else


        <div class="content">
            <div class="content-inside">
                <div class="offer-section">
                    <div class="blockin">
                        <div class="row row-style">
                            <div class="col-md-5 col-lg-6 column-style d-none d-lg-block">
                                <div class="inside__column_left_offer">
                                    <img src="{{asset('images/offer-illustration.svg')}}" alt="">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 column-style">
                                <div class="inside_column_right_offer">
                                    <h1 class="text-shadow">
                                        Отмывайте криптовалюту и оставайтесь в тени с <span
                                            class="font-yellow">whitener</span>
                                    </h1>
                                    <p>
                                        Получайте самые чистые монеты на криптовалютных <br/>
                                        биржах Европы, Азии и Северной Америки
                                    </p>
                                    <div class="form form_offer">
                                        <div class="title_form">
                                            Введите свой биткойн кошелек по адресу ниже:
                                        </div>
                                        @if($errors->any())
                                            @foreach($errors->all() as $error)
                                                <div class="color-red text-left">
                                                    {{ $error }}
                                                </div>
                                            @endforeach
                                        @else
                                            <div class="color-red text-left">
                                                {{ $error }}
                                            </div>
                                        @endif
                                        <form method="post" action="{{route('main.store')}}">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="forward_addr"
                                                       placeholder="Введите первый адрес*" required="required">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="forward_addr2"
                                                       placeholder="Введите второй адрес (опционально)">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="coupon"
                                                       placeholder="Ваучер (опционально)">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="form-submit btn btn-default"
                                                       name="submit_mix" value="Очистить мои монеты">
                                            </div>
                                        </form>
                                        <div class="dop_info_form">

                                            Время возврата {{$info['withdraw_max_timeout']}} часов | Комиссия за обслуживание: {{$info['mixer_fee_pct']}}% + {{$info['mixer_fix_fee']}} BTC |
                                            Сгенерированный адрес действителен в течение {{$info['order_lifetime']}} дней (см. FAQ)
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content">
            <div class="content-inside">

                <section id="how_it_works" class="how_it_works">

                    <div class="hiw__blockin">

                        <div class="row">
                            <div class="col-md-6 col-xl-7 hiw__column d-none d-md-block">

                                <div class="inside__column_hiw">
                                    <img src="images/hiw-illustration.svg" class="hiw__column_img" alt="">
                                </div>

                            </div>
                            <div class="col-md-5 hiw__column">

                                <div class="inside__column_hiw inside__column_right_hiw">
                                    <h2>
                                        Как это работает?
                                    </h2>
                                    <img src="images/steps.svg" class="hiw__steps_img" alt="">
                                    <p>
                                        Биткойн псевдоанонимен, все транзакции записываются в блокчейн. Любой человек
                                        может получить доступ к истории денежных переводов с одного адреса на другой.
                                        Наш сервис дает вам возможность защитить вашу анонимность.
                                    </p>
                                    <p>
                                        <strong class="font-blue">Мы применяем инновационный алгоритм Bitcoin Mixer
                                            2.0</strong> для повышения анонимности и смешивания денег по сравнению с
                                        классическими миксерами.
                                    </p>
                                    <p>
                                        Основным преимуществом нашего сервиса является то, что <strong
                                            class="font-blue">все средства, возвращаемые вам после процедуры
                                            микширования, являются проверенными монетами с бирж криптовалют</strong>,
                                        имеющими несомненно положительную историю.
                                    </p>
                                    <p>
                                        Дополнительным моментом является то, что вы получаете все свои деньги разными
                                        частями в случайные промежутки времени и на разные адреса. Вероятно, это лучший
                                        алгоритм анонимности на сегодняшний день.
                                    </p>
                                </div>

                            </div>
                        </div>

                    </div>

                </section>

                <section id="advantages" class="advantages">

                    <div class="blockin">

                        <div class="title_section">
                            <h2>
                                Наши преимущества
                            </h2>
                        </div>

                        <div class="row adv__row">

                            <div class="col-md-6">

                                <div class="adv__column_inside">

                                    <div class="adv__img">
                                        <img src="images/icons/advantage_img-1.svg">
                                    </div>

                                    <div class="adv__content">
                                        <h3>
                                            Алгоритм: Bitcoin Mixer 2.0
                                        </h3>
                                        <p>
                                            Мы взяли лучшее из существующих биткойн-миксеров, разработали новый алгоритм
                                            анонимизации криптовалюты, добавили возможность получения проверенной
                                            криптовалюты с фондовых бирж Северной Америки, Европы и Азии.
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="adv__column_inside">

                                    <div class="adv__img">
                                        <img src="images/icons/advantage_img-2.svg">
                                    </div>

                                    <div class="adv__content">
                                        <h3>
                                            Без регистрации, без логов
                                        </h3>
                                        <p>
                                            Мы не храним логи, вся необходимая информация для обработки транзакций
                                            удаляется сразу после завершения работы и подтверждения транзакции или после
                                            истечения срока жизни адреса для невыполненных запросов.
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="adv__column_inside">

                                    <div class="adv__img">
                                        <img src="images/icons/advantage_img-3.svg">
                                    </div>

                                    <div class="adv__content">
                                        <h3>
                                            Мы доступны 24x7x365
                                        </h3>
                                        <p>
                                            Наши алгоритмы полностью автоматизированы, а наш сервис работает
                                            круглосуточно. Мы предоставляем нашим клиентам постоянную поддержку. В
                                            случае возникновения вопросов мы стремимся оперативно их решать.
                                        </p>
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="adv__column_inside">

                                    <div class="adv__img">
                                        <img src="images/icons/advantage_img-4.svg">
                                    </div>

                                    <div class="adv__content">
                                        <h3>
                                            Гарантийные письма
                                        </h3>
                                        <p>
                                            Мы предоставляем гарантийные письма с цифровой подписью, которые
                                            подтверждают стопроцентные обязательства перед вами в отношении наших услуг.
                                            Сохраняйте гарантийные письма. Это дает дополнительную уверенность в
                                            разрешении любых споров, которые могут возникнуть.
                                        </p>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </section>

                <section id="faq" class="questions">

                    <div class="blockin">

                        <div class="title_section">
                            <h2>
                                Часто задаваемые вопросы
                            </h2>
                        </div>

                        <div class="row questions__row">
                          @foreach($models as $model)

                            <div class="col-md-6 questions__column">
                                <div class="questions_text">
									<span class="question_info">
										{{$model->question}}
									</span>
                                    <span class="plus"></span>
                                    <div class="questions_content">
                                        {{$model->answer}}
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>

                    </div>

                </section>

                <section id="contacts" class="contacts">

                    <div class="blockin">

                        <div class="title_section">
                            <h2>
                                Контакты
                            </h2>
                        </div>

                        <div class="row contact__row">
                            <div class="col-md-6 contact__row">
                                <p class="contact__text">
                                    Мы всегда открыты для общения, вы можете найти наши <br/>
                                    официальные темы по следующим ссылкам:
                                </p>
                                <div class="contact__item">
                                    <a href="bitcointalk.org/index.php?topic=4667343" class="contact__btn">
										<span class="contact__wrap_icon contact__btn_yellow">
											<img src="images/icons/bitcoin.svg" class="contact__icon" alt="">
											<span class="contact_text">
												Bitcoin
											</span>
										</span>
                                        <span class="contact__link">
											bitcointalk.org/index.php?topic=4667343
										</span>
                                    </a>
                                </div>
                                <div class="contact__item">
                                    <a href="reddit.com/user/Jambler_io" class="contact__btn">
										<span class="contact__wrap_icon contact__btn_red">
											<img src="images/icons/reddit.svg" class="contact__icon" alt="">
											<span class="contact_text">
												Reddit
											</span>
										</span>
                                        <span class="contact__link">
											reddit.com/user/Jambler_io
										</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 contact__row">
                                <p class="contact__text">
                                    Мы всегда готовы ответить на все ваши вопросы. Не стесняйтесь <br/>
                                    обращаться к нам в любое время по адресу:
                                </p>
                                <div class="contact__item">
                                    <a href="t.me/jambler" class="contact__btn">
										<span class="contact__wrap_icon contact__btn_light_blue">
											<img src="images/icons/telegram-white.svg" class="contact__icon" alt="">
											<span class="contact_text">
												Telegram
											</span>
										</span>
                                        <span class="contact__link">
											t.me/jambler
										</span>
                                    </a>
                                </div>
                                <div class="contact__item">
                                    <a href="mailto:support@jambler.io" class="contact__btn">
										<span class="contact__wrap_icon contact__btn_blue">
											<img src="images/icons/email.svg" class="contact__icon" alt="">
											<span class="contact_text">
												Email
											</span>
										</span>
                                        <span class="contact__link">
											support@jambler.io
										</span>
                                    </a>
                                </div>
                                <div class="contact__item">
                                    <a href="#" class="contact__btn">
										<span class="contact__wrap_icon">
											<img src="images/icons/key.svg" class="contact__icon" alt="">
											<span class="contact_text">
												Ключ PGP
											</span>
										</span>
                                        <span class="contact__link">
											B8A5 CFCA F63F F2D8 384A 6B12 D3B2 8095 6F0E 7CAF
										</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>

                </section>

            </div>
        </div>
    @endisset

@endsection
