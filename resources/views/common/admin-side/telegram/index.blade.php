@extends($url_prefix.'.layout.layout')

@section('page-title','Profile')

@section('content-main')
    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                            Настройки телеграм
                        </h4>
                    </div>
                    <form action="{{ route($url_prefix.'.telegram') }}" enctype="multipart/form-data"
                          method="post">
                        @csrf
                        <div class="card-content">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <x-inputs.form-checkbox :model="$model" attribute="status"/>
                                </div>


                                <div class="col-lg-6 col-md-6">
                                    <x-errors :errors="$errors"/>



                                        <!-- TELEGRAM BOT -->
                                        <strong class="center-text">Изображение для приветствия</strong>
                                        @if(!$model->image)
                                            <img src="{{ asset('/img/placeholder.jpg') }}" alt="shop logo"
                                                 id="shop-logo"
                                                 class="thumbnail">
                                        @else
                                            <img src="{{ asset($model->logo()) }}" alt="shop logo" id="shop-logo"
                                                 class="thumbnail">
                                        @endif
                                        <input type="file" id="settings-logo" class="hidden" name="image"
                                               value="">

                                        <div class="col-md-12">
                                            <input type="hidden" name="delete_image" id="delete_image_value" value="0">
                                            <a class="btn btn-danger delete-image @if(!$model->image) hidden @endif "
                                               href="{{ route($url_prefix.'.image') }}">
                                                Удалить
                                            </a>
                                        </div>

                                        <div class="cleafix"></div>
                                        <br>
                                        <x-inputs.form-input :model="$model" attribute="token"/>


                                        <x-inputs.form-textarea :model="$model" attribute="text"/>

                                        <div class="text-center">
                                            <button type="submit" class="btn btn-success btn-fill btn-wd">Сохранить
                                            </button>
                                        </div>
                                        <div class="clearfix"></div>


                                </div>


                            </div>

                        </div>
                    </form>

                </div>

            </div>


        </div>
    </div>
@endsection

@push('scripts')
    <script>

        imagePreview('#settings-logo', '#shop-logo');
        $('.delete-image').on('click', function (e) {
            e.preventDefault();
            $('#settings-logo').val('');
            $('#delete_image_value').val(1);
            $(this).addClass('hidden');
        });

        $('#settings-logo').on('image:uploaded', function () {
            $('.delete-image').removeClass('hidden');
        });


        (function ($) {

            const val = $('#telegram-type').val();

            function hideAllContainers() {
                $('#telegram-bot-container').hide();
            }

            function showTelegramContainer(val) {
                hideAllContainers();
                $('#telegram-' + val.toLowerCase() + '-container').show();
            }


            // hide all telegram containers
            hideAllContainers();

            // show current container
            showTelegramContainer(val);


        })(jQuery);


    </script>
@endpush
