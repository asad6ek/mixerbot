<form method="POST" action="{{ $action }}">
    @csrf
    @method($method)
    <div class="card-content">
        <x-errors :errors="$errors"/>

        <x-inputs.form-input :model="$model" attribute="question"/>
        <x-inputs.form-input :model="$model" attribute="answer"/>
        <x-inputs.form-checkbox :model="$model" attribute="status"/>

        <div class="text-center">
            <button type="submit" class="btn btn-fill btn-success">Сохранить</button>
        </div>
    </div>


</form>
