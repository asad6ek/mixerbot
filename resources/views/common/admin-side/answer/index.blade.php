@extends($url_prefix.'.layout.layout')

@section('page-title','Coupon')

@section('content-main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <a class="btn btn-success " href="{{ route($url_prefix.'.answer.create') }}">
                            Добавить
                        </a>

                    </div>
                    <div class="card-content">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th>
                                    Question
                                </th>
                                <th>
                                    Answer
                                </th>
                                <th>
                                    Статус
                                </th>

                                <th>
                                    Дата создания
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @forelse($models as $model)
                                <tr>

                                    <td>
                                        {{ $model->question }}
                                    </td>
                                    <td>
                                        {{ $model->answer}}
                                    </td>

                                    <td>
                                        <x-status :status="$model->status"></x-status>

                                    </td>

                                    <td>
                                        {{ $model->created_at }}
                                    </td>

                                    <td class="td-actions text-right">

                                        <a class="btn btn-success"
                                           href="{{ route($url_prefix.'.answer.edit', $model->id) }}"
                                           title="Изменить">
									<span class="btn-label">
										<i class="ti-pencil-alt"></i>
									</span>
                                            Изменить
                                        </a>
                                        <a class="btn btn-danger"
                                           href="{{ route($url_prefix.'.answer.delete', $model->id) }}"
                                           title="Удалить"
                                           onclick="window.deleteWithConfirmation(event)">
									<span class="btn-label">
										<i class="ti-trash"></i>
									</span> Удалить
                                        </a>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="7">
                                        @lang('messages.nothing_found')
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        {!!  $models->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
