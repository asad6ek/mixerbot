<form method="POST" action="{{ $action }}">
    @csrf
    @method($method)
    <div class="card-content">
        <x-errors :errors="$errors"/>
        <div class="form-group">
            <label>
                Api
            </label>

            <select name="api_id" class="form-control">
                <option value="">
                    Choose api
                </option>

                @if($apiList)
                    @foreach($apiList as $option)
                        <option value="{{$option->id}}">
                            {{$option->api_key}} ({{$option->name}})
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
        <x-inputs.form-input :model="$model" attribute="number"/>
        <div class="text-center">
            <button type="submit" class="btn btn-fill btn-success">Сохранить</button>
        </div>
    </div>


</form>
