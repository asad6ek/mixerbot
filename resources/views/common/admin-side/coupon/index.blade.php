@extends($url_prefix.'.layout.layout')

@section('page-title','Coupon')

@section('content-main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <a class="btn btn-success " href="{{ route($url_prefix.'.coupon.create') }}">
                            Добавить
                        </a>

                    </div>
                    <div class="card-content">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th>
                                    Купон
                                </th>
                                <th>
                                    Api
                                </th>
                                <th>
                                    Статус
                                </th>

                                <th>
                                    Дата активации
                                </th>

                                <th>
                                    Дата создания
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @forelse($models as $model)
                                <tr>

                                    <td>
                                        {{ $model->code }}
                                    </td>
                                    <td>
                                        {{ $model->api? $model->api->name() :'' }}
                                    </td>

                                    <td>
                                        @if($model->isUsed())
                                            <span class="label label-success">Опубликован</span>
                                        @else
                                            <span class="label label-danger">Не опубликован</span>
                                        @endif
                                    </td>

                                    <td>
                                        {{ $model->used_at }}
                                    </td>
                                    <td>
                                        {{ $model->created_at }}
                                    </td>

                                    <td class="td-actions text-right">


                                        <a class="btn btn-danger"
                                           href="{{ route($url_prefix.'.coupon.delete', $model->id) }}"
                                           title="Удалить"
                                           onclick="window.deleteWithConfirmation(event)">
									<span class="btn-label">
										<i class="ti-trash"></i>
									</span> Удалить
                                        </a>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="7">
                                        @lang('messages.nothing_found')
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                        {!!  $models->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
