@extends($url_prefix.'.layout.layout')

@section('page-title','Добавить coupon')


@section('content-main')
	<div class="container-fluid">
		<div class="row">
            <div class="col-md-12 " style="padding: 15px">
                <a href="{{route($url_prefix.'.coupon.index')}}" class="btn btn-warning btn-fill btn-move-left">
													<span class="btn-label">
															<i class="ti-angle-left"></i>
													</span>
                    Назад
                </a>
            </div>
			<div class="col-md-6">
				<div class="card">
					<div class="card-header">
						<h3 class="card-title">Generate coupons</h3>
					</div>
					<div class="card-content">
						@includeFirst([$url_prefix.'.coupon.form','common.admin-side.coupon.form'],[
							'method'=>'POST',
							'action'=>route($url_prefix.'.coupon.store'),
							'model'=>$model,
							'apiList'=>$apiList,
						])
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
