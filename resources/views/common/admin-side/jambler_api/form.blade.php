<form method="POST" action="{{ $action }}">
	@csrf
	@method($method)
	<div class="card-content">
		<x-errors :errors="$errors"/>
		<x-inputs.form-input :model="$model" attribute="name"/>
		<x-inputs.form-input :model="$model" attribute="api_key"/>
		<div class="text-center">
			<button type="submit" class="btn btn-fill btn-success">Сохранить</button>
		</div>
	</div>


</form>
