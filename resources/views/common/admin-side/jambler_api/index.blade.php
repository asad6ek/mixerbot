@extends($url_prefix.'.layout.layout')

@section('page-title','API')

@section('content-main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">

                        <a class="btn btn-success " href="{{ route($url_prefix.'.jambler_api.create') }}">
                            Добавить
                        </a>

                    </div>
                    <div class="card-content">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>

                                <th>
                                    Название
                                </th>

                                <th>
                                    Jambler api keys
                                </th>

                                <th>
                                    Дата добавления
                                </th>

                                <th>

                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @forelse($models as $model)
                                <tr>

                                    <td>
                                        {{ $model->name }}
                                    </td>
                                    <td>
                                        {{ $model->api_key }}
                                    </td>
                                    <td>
                                        {{ $model->created_at }}
                                    </td>

                                    <td class="td-actions text-right">

                                        @if($model->default === 0)     <a class="btn btn-info"
                                                     href="{{ route($url_prefix.'.jambler_api.default', $model->id) }}"
                                                     title="По умолчанию">
									<span class="btn-label">
										<i class="ti-location-pin"></i>
									</span>
                                            По умолчанию
                                        </a>
                                        @else
                                            <button type="button" class="btn btn-success btn-fill btn-wd" disabled>
                                             <span class="btn-label">
                                                 <i class="fa fa-check"></i>
                                             </span>
                                                По умолчанию
                                            </button>
                                        @endif
                                        <a class="btn btn-success"
                                           href="{{ route($url_prefix.'.jambler_api.edit', $model->id) }}"
                                           title="Изменить">
									<span class="btn-label">
										<i class="ti-pencil-alt"></i>
									</span>
                                            Изменить
                                        </a>


                                        <a class="btn btn-danger"
                                           href="{{ route($url_prefix.'.jambler_api.delete', $model->id) }}"
                                           title="Удалить"
                                           onclick="window.deleteWithConfirmation(event)">
									<span class="btn-label">
										<i class="ti-trash"></i>
									</span> Удалить
                                        </a>

                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="7">
                                        @lang('messages.nothing_found')
                                    </td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                                                {!!  $models->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
