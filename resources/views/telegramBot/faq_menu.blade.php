<strong>Q:</strong> What is the minimum and maximum amount you accept for cleansing?
<strong>A:</strong> From 0.001 up to 50 BTC.

<strong>Q:</strong> How many confirmations do you require on my incoming transfer?
<strong>A:</strong> We start processing your order after the first confirmation.

<strong>Q:</strong> How much time do I have to send my coins after placing a mixing request?
<strong>A:</strong> The input address we give you will remain valid for 7 days after your request.

<strong>Q:</strong> What logs do you store?
<strong>A:</strong> None, only the temporary data required to process your order, and only until it is completed.

<strong>Q:</strong> If so, then how can I prove I sent you coins if something goes wrong?
<strong>A:</strong> After placing your request, we send you our PGP-signed letter of guarantee, containing all the details of our deal, including the input address for your coins. Save and keep this letter; it will be an infallible proof of our obligations in case you suspect any malfunction or fraud.
