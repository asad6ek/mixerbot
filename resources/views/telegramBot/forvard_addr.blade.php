@if ($order->hasError())

    @if($errors->any())
        @foreach($errors->all() as $error)
            {{ $error }}
        @endforeach
    @else
        {{ $error }}
    @endif
@else
    If you wish to use a second forward address, please enter it now. Otherwise, click Start Mixing.
@endif

