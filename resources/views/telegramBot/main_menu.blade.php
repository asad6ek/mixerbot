@isset($info['err'])
    {{ $info['err'] }}
@else
{{$info['mixer_name']}}
Welcome. We are an innovative, premium level bitcoin mixer. Receive clean coins from cryptocurrency stock exchanges around the globe.
Commission: {{$info['mixer_fee_pct']}}% + {{$info['mixer_fix_fee']}} BTC
Cleansing time: up to {{$info['withdraw_max_timeout']}} hours.
A unique anonymization algorithm.
<strong>Clearnet:</strong> https://jambler.io
<strong>Tor:</strong> jambler2zgfnjykq.onion
@endisset
