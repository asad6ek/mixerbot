<?php

use Illuminate\Support\Facades\Route;


Route::prefix('auth')->name('admin.auth.')->group(function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('remote/{user}/{token}', 'Auth\CustomController')->name('remote');
});


Route::name('admin.')->middleware('admin')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::resource('api', 'JamblerApiController')->names([
        'index' => 'jambler_api.index',
        'create' => 'jambler_api.create',
        'store' => 'jambler_api.store',
        'edit' => 'jambler_api.edit',
        'update' => 'jambler_api.update',
        'destroy' => 'jambler_api.delete',
    ]);
    Route::resource('answer', 'AnswerController')->names([
        'index' => 'answer.index',
        'create' => 'answer.create',
        'store' => 'answer.store',
        'edit' => 'answer.edit',
        'update' => 'answer.update',
        'destroy' => 'answer.delete',
    ]);
Route::get('api/{id}/default','JamblerApiController@defaultUpdate')->name('jambler_api.default');
    Route::resource('coupon', 'CouponController')->names([
        'index' => 'coupon.index',
        'create' => 'coupon.create',
        'store' => 'coupon.store',
        'edit' => 'coupon.edit',
        'update' => 'coupon.update',
        'destroy' => 'coupon.delete',
    ]);


    Route::get('telegram', 'TelegramSettingController@index')->name('telegram');
    Route::delete('image', 'SettingsController@deleteImage')->name('image');

    Route::post('telegram', 'TelegramSettingController@store');

});
