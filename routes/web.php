<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::post('/telegram/{token}', 'Telegram\TelegramController@process')
    ->name('telegram.bot');
Route::name('main.')
    ->namespace('Main')
    ->group(function () {

        Route::get('/', 'MixerController@create')->name('index');
//        Route::get('/mix', 'MixerController@create')->name('mix');
        Route::post('/','MixerController@store')->name('store');
        Route::post('/download','MixerController@download')->name('guarantee');
    });
