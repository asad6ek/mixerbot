<?php

return [
    'main_site' => env('MAIN_SITE', 'localhost'),
    'date' => [
        'format' => env('DATE_FORMAT', 'd.m.Y H:i'),
    ],
];
